; D16PATCH - A 16-bit "in memory patching" utility for DOS

; Copyright (c) 2021-2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm d16patch.asm D16PATCH.COM
; Building with NASM requires small changes in the code, especially the
;   syntax of "if" commands. See also the comment at wraparound_fix_1.

; See README.MD about the details on input file format

; Important note before adding new features: The size of the program
;   should remain below 6144 bytes - 1536 (stack) - 2560 (patch buffer)
;   - 128 (exec cmdline args buf) - 80 (EXE path) - 256 (PSP) = 1584 bytes.
; However, this is not a "strict" limit, it is very unlikely that using a
;   few hundreds of bytes smaller stack will cause any problem

; If the value of the following switch is nonzero, 
;   "wraparound fix" code will not be assembled
no_wraparound_fix equ 0
; Skip DOS 1 compatibility code, saves a few bytes
no_dos_1_compat_code equ 0

; Offset of command line arguments in the PSP
psp_offs_cmd_line_args equ 80h
; The position ofter the size byte
psp_offs_cmd_line_args_start equ 81h
; The end of the same block
psp_offs_cmd_line_args_end equ 100h
; Limit the memory usage of the program to 6 KB before executing game.
;   However, the stack pointer will be relocated at the program start
;   The memory area after the new stack will be used as a read buffer
mem_paragraphs equ 180h
; The read buffer
read_buf_pos equ (mem_paragraphs * 16)
; the new value for stack pointer
stack_realloc_sp equ (read_buf_pos - 2h)
; The patch file size is limited to 48 KB - 1 byte (a terminator byte
;   will be appended.) However, the program will read one byte more
;   for checking if the file is too large
read_buf_size equ (48 * 1024)
; The maximim patch file size, see comment above
patch_file_size_max equ (read_buf_size - 1)
; Size of the char array storing the executable file name.
exe_file_path_arr_size equ 50h
; The start of the data block storing the executable file name
exe_file_path_arr_pos equ data_area_start
; Maximum length of EXE file path in input file (minus \0 terminator)
exe_file_path_len_max equ (exe_file_path_arr_size - 1)
; Number of bytes reserved for a command line arguments block (for exec)
cmdline_args_data_size equ 80h
; The start of the command line arguments data block
cmdline_args_data_pos equ (exe_file_path_arr_pos + exe_file_path_arr_size)
; The offset of end of the command line arguments data block
cmdline_args_data_end equ  (cmdline_args_data_pos + cmdline_args_data_size)
; Size of patch_data_block (currently 2.5 KB)
patch_data_block_size equ  0a00h
; Start position of "patch data" block
patch_data_pos equ  cmdline_args_data_end
; The end position of the same block. Note that this corresponds to
;   a maximal size, the actual size and end pos is usually smaller
patch_data_block_end equ (patch_data_pos + patch_data_block_size)
; Size of an entry header in patch data (see ....)
patch_data_entry_head_size equ 3h
; Maximum size of the "data" part of the patch
patch_data_entry_data_max_size equ 10h
; For temporary storage of input file name use the read buffer
patch_file_name_temp_buf equ read_buf_pos
; Length of input file signature
ingut_sign_size equ (input_file_sign_end - input_file_sign)
; Minimum file size, checked after read
min_file_size equ (ingut_sign_size + 2h)

; Exit error codes 
; Note that "unusual" error codes were chosen which are
;   unlikely to be used by the child process 
; No error
exit_code_ok equ 0h
; Any other error
exit_code_error_other equ 0f0h
; Command line arguments error
exit_code_error_args equ 0f1h
; I/O error
exit_code_error_io equ 0f2h
; Input (patch) file format error
exit_code_error_file_format equ 0f3h
; DOS execute error
exit_code_error_dos_exec equ 0f4h
; Patching failure
exit_code_error_patch_failure equ 0f9h

; Number of interrupt vector used to store the DOS
;   program termination address
int_no_dos_termination_addr equ 22h
; Offset of the "termination address" in PSP
psp_offs_termination_addr equ 0ah
; Error value used bx decode_hex_digit
hex_decode_error equ 0ffh

use16
org 100h

prog_start:
    mov     sp, stack_realloc_sp

check_dos_ver:
    ; Check DOS version, DOS exec may not restore ss, sp in older versions
    ; int 21h, ah = 30h Get DOS version
    ; If version = 1.0 al, should not change
    mov     ax, 3000h
    xor     bx, bx
    int     21h
    ; Versions >= 3.0 should be OK
    cmp     al, 3h
    jae     short dos_ver_ok
    mov     dx, str_dos_ver_err
    ; DOS 1.0 compatibility code, can be removed
if no_dos_1_compat_code = 0
    or      al, al
    jnz     short dos_ver_2
    mov     ah, 9h
    int     21h
    int     20h
end if
dos_ver_2:
    mov     bl, exit_code_error_other
    jmp     exit_error_2
dos_ver_ok:

    ; Now, check the program segment prefix for the arguments
    ;   arg #1 is the input file name, everything else will be passed
    ;   to the program. The file name will be copied to
    ;   the memory area starting at patch_file_name_temp_buf
    ; Note: this is a com program, assumes cs == ds == es
    cld
    mov     di, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [di-1h]
    ; Report an error if there are no command line arguments
    jcxz    arg_parse_error
    ; skip spaces
    mov     al, 20h
    repe    scasb
arg_parse_check_switch:
    ; Check if a /e or /E switch was provided - 
    ;   - Execute the program even if patching has failed
    ; First check if there is at least 4 characters remaining
    ;   (2 chars of switch + space)
    ; Note: di points to the byte after this and cx also contains the
    ;    remaining bytes from this pos. dec di / inc cx comes only later
    ; If not, skip this check to avoid buffer over-read
    cmp     cx, 3h - 1h
    jb      short arg_parse_2
    ; Check if the first character is a slash
    cmp     byte [di-1h], '/'
    jne     short arg_parse_2
    ; A switch  was provided, check if it is valid
    ; Check the first letter of switch + the space
    mov     ax, [di]
    ; switch letter in al, upper case it
    and     al, 0dfh
    cmp     ax, 'E '
    je      short arg_parse_1
arg_parse_error:
    ; no arguments provided, error
    mov     dx, str_error_args
    mov     bl, exit_code_error_args
    jmp     exit_error
arg_parse_1:
    ; Advance di and cx, the former will point to the space
    inc     di
    dec     cx
    ; skip spaces, again
    mov     al, 20h
    repe    scasb
    ; Note that we need no check here as we have checked the number
    ;   of bytes remaining above. Set always_execute
    inc     byte [always_execute]
arg_parse_2:
    ; move one byte back (the actual start of arg)
    dec     di
    inc     cx
    ; save the position in si 
    mov     si, di
    ; find the end of the argument
    mov     al, 20h
    repne   scasb
    ; Check if a space was found (the argument may be at the
    ;   end of command line buffer, with no space after it)
    jnz     short arg_parse_3
    ; move one byte backwards but only if a space was found
    dec     di
arg_parse_3:
    ; set the number of bytes to be copied
    mov     cx, di
    sub     cx, si
    ; report an error if the number of bytes is zero
    jz      short arg_parse_error
arg_parse_copy:
    ; Copy the file name to patch_file_name_temp_buf
    mov     di, patch_file_name_temp_buf
    rep     movsb
    ; Add a null terminator
    xor     al, al
    stosb
    
    ; Copy finished, now fill the cmdline_args_data_pos block
    ; Calculate the new length and the number of bytes to copy
    ; New_length(ax) = Old_length - (index(si) - Block_start_81h)
    ; Bytes_to_copy(cx) = Block_end - index(si)
    xor     ah, ah
    mov     al, [psp_offs_cmd_line_args]
    add     ax, psp_offs_cmd_line_args_start
    sub     ax, si
    mov     cx, psp_offs_cmd_line_args_end
    sub     cx, si
    mov     di, cmdline_args_data_pos
    ; store the size - al
    stosb
    ; copy the remaining bytes in the cmd line args part of PSP
    rep     movsb
    ; Store a terminator character (0dh)
    ; Note: If the PSP of the program has a bad format (e.g. lack of
    ;   terminator), it may write a few bytes into the patch buffer
    mov     al, 0dh
    stosb
    ; fill the remaining bytes of cmdline_args_data_pos with 0
    mov     cx, cmdline_args_data_end
    sub     cx, di
    xor     al, al
    rep     stosb
    
    ; Now, read the patch file. The buffer starts at read_buf_pos, this
    ;   corresponds to a memory area that will be "freed" before executing
    ;   the program. The maximum allowed size is patch_file_size_max (one
    ;   byte less than the buffer size, so we can add a "terminator".
    ;   This is also useful for checking if the file is too large.
    ; See usage of read_file there
    mov     bx, patch_file_name_temp_buf
    mov     cx, read_buf_size
    mov     dx, read_buf_pos
    call    read_file
    
    ; Check the number of bytes read. The maximum allowed is
    ;   read_buf_size - 1! "File too long" is considered a file format
    ;   error, not an I/O error. Also check for empty data
    cmp     ax, cx
    jae     patch_file_size_error
    ; Minimum file size check modified
    cmp     ax, min_file_size
    jae     short patch_file_size_ok
patch_file_size_error:
    ;; mov     dx, str_error_file_format
    ;; mov     bl, exit_code_error_file_format
    ;; jmp     exit_error ; save a few bytes
    jmp     patch_decode_error_1
patch_file_size_ok:
    
    ; Add a terminator. at the beginning, the terminator is a (\n) 0xa byte
    ;   which will be
    ; This makes the implementation of comment preprocessing a lot easier,
    ;   but also very useful in the second, processing step
    ; Store the end of buffer in dx
    mov     si, dx
    add     dx, ax
    mov     bx, dx
    mov     byte [bx], 0ah
    inc     dx
    ; The patch file is processed in 2 steps. The first step is "pre-
    ;   processing". In this stage, all whitespace characters are replaced
    ;   with spaces, hex numbers are upper cased and comments are removed.
    ; End of buffer in dx. Note that size = 0 check must be before this loop
    
preprocess_loop:
    lodsb
    ; Step 1. Replace \t (0x9), \n (0xa), and \r (0xd) with spaces
    cmp     al, 9h
    je      short preprocess_loop_1
    cmp     al, 0ah
    je      short preprocess_loop_1
    cmp     al, 0dh
    jne     short preprocess_loop_2
preprocess_loop_1:
    mov     al, 20h
    jmp     preprocess_loop_modify
preprocess_loop_2:
    cmp     al, '#'
    jne     short preprocess_loop_3
    ; A comment, replace all chars with spaces until the next \n char
    ; set cx to the number of bytes remaining in buffer
    mov     cx, dx
    sub     cx, si
    mov     di, si
    ; "Hack" si to point to the '#' char
    dec     si
    mov     al, 0ah
    repne   scasb
    ; Note - we should always find a 0xa (\n) byte as one was 
    ;   appended to the buffer as a terminator
    mov     cx, di
    sub     cx, si
    mov     di, si
    ; Replace the entire comment with spaces
    mov     al, 20h
    rep     stosb
    ; set si (position after newline char after the comment)
    mov     si, di
    ; Note that the preprocess_loop_modify step must be skipped in this case
    jmp     preprocess_loop_test
preprocess_loop_3:
    ; 'a'-'z' to upper case
    cmp     al, 'a'
    jb      short preprocess_loop_test
    cmp     al, 'z'
    ja      short preprocess_loop_test
    and     al, 0dfh
preprocess_loop_modify:
    ; modify the character
    mov     [si-1h], al
preprocess_loop_test:
    cmp     si, dx
    jb      short preprocess_loop
    ; Now, start the actual decoding of the data.
    ; Note: the end offset of array is still in register dx.
patch_decode_start:
    ; Check signature for input file. The signature must start at the 
    ;   first byte of the file, no space or comment may precede it.
    ; Note that file signature checking is case insensitive
    ;   due to preprocessing!
    mov     si, read_buf_pos
    mov     di, input_file_sign
    mov     cx, ingut_sign_size
    repe    cmpsb
    jnz     short patch_decode_error_1
    ; Skip spaces
    call    skip_spaces
    ; End of block - error
    jc      short patch_decode_error_1
    ; The first entry after signature  in the file is the path to
    ;   executable file. (Note: code copied from arg parser)
    ; Note: register di will be set to the (proeproc.) read buffer (not si!)

    ; set di to the current position in buffer. si will store the start
    ;    offset of the name in the buffer
    mov     di, si
    ; find the end of the argument
    mov     al, 20h
    repne   scasb
    ; Check if a space was found- ZF=0 means that there si no space until
    ;   the end of block. This should not happen
    ;   (as we have added a "terminator").
    jnz     short patch_decode_error_1
    ; move one byte backwards
    dec     di
    ; set the number of bytes to be copied
    mov     cx, di
    sub     cx, si

    ; No cx = 0 check, this should never happen
    ; Executable path must be at most 79 characters long and may
    ;     not contain spaces, tabs and '#'
    cmp     cx, exe_file_path_len_max
    jbe     short patch_decode_copy_exec_path
patch_decode_error_1:
    mov     dx, str_error_file_format
    mov     bl, exit_code_error_file_format
    jmp     exit_error
patch_decode_copy_exec_path: 
    mov     di, exe_file_path_arr_pos
    rep     movsb
    ; Append a \0 terminator
    xor     al, al
    stosb
    ; Skip spaces
    call    skip_spaces
    ; End of block - error
    jc      short patch_decode_error_1
    ; The next entry is a single byte - the number of "exe file check"
    ;     entries. Format is a single hex digit, followed by a space.
    lodsb
    call    decode_hex_digit
    jc      short  patch_decode_error_1
    ; Check if the next character is a space
    cmp     byte [si], 20h
    jne     short  patch_decode_error_1
    ; Store the hex digit in num_exec_checks
    mov     [num_exec_checks], al
    ; Set di to the output buffer
    mov     di, patch_data_pos
    ; Save bp (commented out, bp is currently not used for any other purpose)
    ; If bp must be preserved ; push    bp
    ; The "executable checks" and the patches are stored in the same buffer
    ;   (starts patch_data_pos). num_exec_checks is th number of items in
    ;   the  first part. The end offset is stored at patch_data_end.
    ;   Each "check"/patch entry has a header of 3 bytes, followed by 
    ;   the patch data itself (1-16 bytes).
    ; header, bytes 0-1: bits 15-0 of the patching data, the offset
    ; header, byte 2, bits 7-4: bits 19-16 of the 20-bit address
    ; header, byte 2, bits 3-0: the length of the patch minus 1
    ;   (unless wraparound fix is applied)
    ; The patches can also contain a segment indicator '$'. These segments
    ;   will be modified after the program is loaded into memory and the
    ;   code segment of it will be known. The offsets where such modifications
    ;   must be done are stored at the end of patch buffer. When a new segment
    ;   indicator is found, buffer_patches_end is decremented, and the offset
    ;   in the patch buffer is saved there.
patch_decode_loop:
    ; "Protection" against misplaced segment indicators. A '$' sign before
    ;   the last byte of a patch (e.g. 1234$56) is invalid (a segment is 
    ;   2 bytes long) and without a check the first byte of next patch would
    ;   be overwritten during the "patching" phase (i.e. when the patched
    ;   program is already loaded, but not started yet.) Note that other
    ;   invalid uses of '$' do not require additional checks. 
    lea     ax, [di - 1h]
    cmp     [offs_last_patch], ax
    je      short patch_decode_error_2
    ; Decode the remaining patches. The format is: (spaces) 20 bit address
    ;   (spaces) byte sequence in hexadecimal format (2-32 hex digits)
    ; Skip spaces first. As we have added a "terminator" in the buffer
    ;   we van be sure that it ends with a space
    call    skip_spaces
    ; End of data. This is not an error, but the actual end of the data!
    jc      short patch_decode_end
    ; Check if the new header will fit in the block (+ at least 1 byte!)
    lea     ax, [di + patch_data_entry_head_size]
    cmp     [buffer_patches_end], ax
    jbe     short patch_decode_error_2
    ; An address first
    call    decode_20_bit_addr
    jc      short patch_decode_error_2
    ; store the header in the patch block
    stosw
    ; Save the offset of byte #2 of the "header" part into bp
    mov     bx, di  
    mov     al, cl
    stosb
    ; The spaces after the segment. If the file neds here, this is an error
    call    skip_spaces
    jc      short patch_decode_error_2
    ; Overflow check. Limit the number of maximum length of the last patch
    ;   if a longer one would not fit in buffer. Note 1: Uses bp instead of 
    ;   bx as the latter is required for setting the low-order 4-bits of
    ;   head byte #2. Note 2: The end position in the buffer depends on
    ;   the number of segments in the patches
    mov     ax, [buffer_patches_end]
    ; Set bp to the maximum possible length of data part
    lea     bp, [di + patch_data_entry_data_max_size]
    ; Check if this means an overflow
    cmp     bp, ax
    jbe     short decode_patch_bytes_loop
    mov     bp, ax
decode_patch_bytes_loop:
    ; Start decoding the patch data. Note: Overflow check was moved
    ;   as the segment code might update bp
    lodsb
    ; Check for a segment indicator
    cmp     al, '$'
    jne     short decode_patch_bytes_seg_end
    mov     ax, [buffer_patches_end]  ; Note - shorter instruction
    xchg    ax, bx  ; Save value of bx in ax, set bx
    dec     bx
    dec     bx
    mov     [bx], di
    mov     [offs_last_patch], di
    xchg    ax, bx  ; Restore bx
    mov     [buffer_patches_end], ax  ; Note - shorter instruction
    ; We might need to update bp if buffer_patches_end was modified
    cmp     bp, ax
    jbe     short decode_patch_bytes_seg_1
    mov     bp, ax
decode_patch_bytes_seg_1:
    lodsb
decode_patch_bytes_seg_end:
    ; Is the sequence longer than the allowed 16 bytes?
    cmp     di, bp
    jae     short patch_decode_error_2
    call    decode_hex_digit
    jc      short patch_decode_error_2
    ; Use register ah for temporary storage of the more 
    ;   significant 4 bits
    mov     ah, al
    mov     cl, 4h
    shl     ah, cl
    lodsb
    call    decode_hex_digit
    jc      short  patch_decode_error_2
    or      al, ah
    stosb
    ; Note that the patch data must contain at least 2 characters.
    ;   Therefore, the check for a whitespace character is done
    ;   _after_ this
    cmp     byte [si], 20h
    ; continue the _outer_ loop
    je      short patch_decode_loop
    ; Note: We must store number_of_bytes - 1 in the lower 4 bits
    ;   of header byte #2. Therefore, we increase it when
    ;   the loop will certainly continue
    inc     byte [bx]
    ; continue the _inner_ loop
    jmp     short decode_patch_bytes_loop
patch_decode_error_2:
    ; If bp must be preserved ; pop     bp
    ;; mov     dx, str_error_file_format
    ;; mov     bl, exit_code_error_file_format
    ;; jmp     exit_error ; save a few bytes
    jmp     patch_decode_error_1
patch_decode_end:
    ; restore bp (commented out, bp is currently not used for anything else)
    ; If bp must be preserved ; pop     bp
    ; Set patch_data_end
    mov     [patch_data_end], di
resize_mem:
    ; Note es: has the correct value
    mov     bx, mem_paragraphs
    mov     ah, 4ah
    int     21h
    jnc     short load_prog
exec_error:
    ; Note uses the same error for memory deallocation and exec failure
    mov     dx, str_error_dos_exec
    mov     bl, exit_code_error_dos_exec
    jmp     exit_error
load_prog:
    ; Load the program. Restore es first (also store cs in ax)
    mov     ax, cs
    mov     es, ax
    ; set segments in "parameters block" (TODO: "magic" numbers)
    mov     [data_param_block + 4h], ax
    mov     [data_param_block + 8h], ax
    mov     [data_param_block + 0ch], ax
    mov     dx, exe_file_path_arr_pos
    mov     bx, data_param_block
    mov     ax, 4b01h
    int     21h
    ; Set stack
    jc      short exec_error

exe_load_ok:
    ; Note: The code assumes that at least registers ss and sp
    ;   are preserved if the program is loaded but not executed.
    ; There is a check at the beginning for DOS 3.0+
    mov     ax, cs
    mov     ds, ax
    ; Apply the segment "relocations", indicated by a '$' in the
    ;   patch file and stored at the end of patch buffer
    mov     dx, [param_block_cs_ip + 2h]
    mov     si, [buffer_patches_end]
    cld
segm_modify_loop:
    cmp     si, patch_data_block_end
    jae     short exe_checks_patch
    lodsw
    mov     di, ax
    add     [di], dx
    jmp     segm_modify_loop
exe_checks_patch:
    ; Now, 1. check if this is the correct executable file using
    ;   the check provided in the patch file; 2. apply the patches
    ; "Checking" and "patching" is done in the same loop. 
    ; Use num_exec_checks as counter in the "checking" phase
    ;   (its value will be destroyed.). If this value reaches 0,
    ;   "patching" begins. Later, this variable will be used for 
    ;   checking whether the "checks" were successful (0 = success).
    ; Restore ds (probably not needed in DOS 3.0+)
    mov     si, patch_data_pos
exe_checks_patch_loop:
    ; Check if we are at the end of "patches" buffer. An invalid number of 
    ;   "checks" (less than the number of entries in the buffer) will be 
    ;   treated as a patching failure, not a file format error.
    cmp     si, [patch_data_end]
    jae     short exec_loaded_program
    ; read first two bytes (offset)and store it in di
    lodsw
    mov     di, ax
    ; Read the third byte (2 if  0-based index) of data
    lodsb
    ; Set cx to the number of bytes to check or modify. 
    ; Note: size - 1 is stored in bits 0-3
    xor     ah, ah
    mov     cx, ax
    and     cl, 0fh
    inc     cx
    ; Set the segment. Note: the byte loaded into al will be moved into ah
    and     al, 0f0h
    xchg    al, ah
    ; The code segment of the "child" program was saved to the exec 
    ;   parameter buffer by int 21h, ax= 4b01h.
    add     ax, [param_block_cs_ip + 2h]
    ; Apply the wraparound fix if needed
if no_wraparound_fix = 0
    call    wraparound_fix
else
    mov     es, ax
end if
    cmp     byte [num_exec_checks], 0h
    jne     short exe_check_loop_check
    rep     movsb
    jmp     exe_checks_patch_loop
exe_check_loop_check:
    repe    cmpsb
    ; Note: The easiest solution I am aware of for unloading the child 
    ;   is calling the DOS exit interrupt 21h, ah = 4ch.
    ; On a check failure, we simply proceed with setting the return
    ;   address. Later we check num_exec_checks again and show an error
    ;   when the checks failed (See the comment at exe_checks_patch_loop)
    jnz     short exec_loaded_program
    dec     byte [num_exec_checks]
    jmp     exe_checks_patch_loop
    
exec_loaded_program:
    ; get PSP of the child process (Use documented, DOS 3.0+ func)
    mov     ah, 62h
    int     21h
    mov     es, bx
    ; Set the return address (saved int 22h, dword at 0ah in PSP)
    mov     ax, prog_return
    mov     [es:psp_offs_termination_addr], ax
    mov     [es:psp_offs_termination_addr + 2h], cs
    ; cli  ; probably not needed
    ; also set int 22h in the interrupt vector table to this
    xor     cx, cx
    mov     es, cx
    mov     [es:int_no_dos_termination_addr * 4h], ax
    mov     [es:int_no_dos_termination_addr * 4h + 2h], cs    
    ; sti  ; probably not needed
    
    ; Check if patching succeeded. On failure, show an error
    ;   message and prompt the user if we should exit or run the
    ;   program without patches
    cmp     byte [num_exec_checks], 0h
    je      short exec_program_set_regs
    ; If always_execute is set, execute the program even in this case
    cmp     byte [always_execute], 0h
    jne     short exec_program_set_regs
    ; Unload the "child" program by simply "exiting" from it
    mov     ax, exit_code_error_patch_failure + 4c00h
    int     21h
exec_program_set_regs:
    ; Set registers for child process, clear interrupts
    ;   so that we can modify ss
    cli
    mov     sp, [param_block_ss_sp]
    mov     ss, [param_block_ss_sp+2h]
    ; Set ES and DS to the PSP segment (stored in bx)
    mov     es, bx
    mov     ds, bx
    ; See www.beroset.com/asm/showregs.asm (+FreeDos Kernel source)
    ; Note: These values are for .COM executables only, for .EXE programs
    ;    this is undefined(?). However, the program will do the same as
    ;    the program was .COM
    mov     ax, 0ffffh
    mov     bx, ax
    mov     cl, 0ffh  ; ch was set to 0 before
    mov     di, sp
    mov     si, 100h
    mov     bp, 91eh
    sti
    ; Now jump to it
    ; Note: JMP DWORD in some other assemblers
    jmp     far [cs:param_block_cs_ip]
    
prog_return:
    ; The executed program returned. Restore the stack
    ; (Note: not absolutely necessary in current program)
    cli
    mov     ax, cs
    mov     ss, ax
    mov     sp, stack_realloc_sp
    mov     ds, ax
    sti
    ; Check num_exec_checks again, print error message on failure
    cmp     byte [num_exec_checks], 0h
    jne     short prog_return_1
    ; Return the exit code of the child process
    ; int 21h, ah = 4dh - get exit code
    mov     ah, 4dh
    int     21h
    jmp     short exit_3
prog_return_1:
    mov     dx, str_patch_failure
    mov     bl, exit_code_error_patch_failure
    ; Note - duplicated because exit_error is used to print
    ;   the string without str_msg_error at end
    mov     ah, 9h
    jmp     exit_error_2
    
    
exit_error:
    ; Print the error text passed in dx, the print str_msg_error
    ;   int 21h, ah=9h - write string
    mov     ah, 9h
    int     21h
    mov     dx, str_msg_error
exit_error_2:
    int     21h
    mov     al, bl
exit_3:
    ;   int 21h, ah=4ch - quit
    mov     ah, 4ch
    int     21h
    
if no_wraparound_fix = 0
wraparound_fix:
    ; Fix memory addresses that would point to the "high" memory if the
    ;   A20 gate was open. Segment is passed in register ax and offset
    ;   in register di. Sets es to the appropriate value, and modifies
    ;   the offset (in register di) if needed. Destroys the value of dx.
    ; If dx must be preserved ; push    dx
    ; uses register dx internally for calculation, but preserves its value
    mov     dx, di
    shr     dx, 1
    shr     dx, 1
    shr     dx, 1
    shr     dx, 1
    add     dx, ax
    ; CF will be set if this address will point to the upper memory
    ;   if the gate is open
    jc      short wraparound_fix_1
    ; BUG FIX: If the offset points to the last bytes of a segment and 
    ;   fewer bytes are remaining than the patch length, the end of the
    ;   patch would be written to the wrong position (start of same seg)
    ; We will use the maximum possible length of the patch in this check
    cmp     di, -patch_data_entry_data_max_size
    jb      short wraparound_fix_end
wraparound_fix_1:
    mov     ax, dx
    ; Bits 19-4 of the 20-bit address are put into the segment
    ;   the offset will only contain the least significant 4 bits
    ; Warning: For the next instruction, NASM will generate an opcode
    ;   that is only documented for 80386+ (0x83/4) and may or may not
    ;   work on older CPUs. 
    ; For building with NASM, change the following line to:
    ;   and     di, strict word 0fh
    and     di, word 0fh
wraparound_fix_end:
    mov     es, ax
    ; If dx must be preserved ; pop     dx
    retn
end if
    
decode_hex_digit:
    ; Decodes a hex digit in in register al. Returns 0xff and sets
    ;   CF on error. Note: case sensitive, accepts 'A'-'F'
    ;   (possible because the input is upper cased in the preprocess phase)
    ; The carry flag will be set if al < 30h
    sub     al, 30h
    jc      decode_hex_digit_error_2
    ; Is the (original value of) input <= '9' ?
    cmp     al, 9h
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    ja      short decode_hex_digit_a_f
decode_hex_digit_ok:
    clc
    retn
decode_hex_digit_a_f:
    ; Digits 'A' - 'F'
    cmp     al, 11h
    ; The carry flag will be set if the original input is < 41h
    jc      short decode_hex_digit_error_2
    sub     al, 7h
    ; Is the (original value of) input <= 'F' ?
    cmp     al, 0fh
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    jbe     short decode_hex_digit_ok
decode_hex_digit_error_1:
    stc
decode_hex_digit_error_2:
    mov     al, hex_decode_error
    retn
    
decode_20_bit_addr:
    ; Decodes a 20-byte address, e.g. 0x123AB. Loads the input bytes using
    ;   the lodsb instruction (start addr.: si). Returns bits 0-15 in ax
    ;   and bits 16-19 in cl (bits 7-4 are used!). Only space is accepted
    ;   as "end" character, otherwise an error is reported. 
    ; Sets CF on error. Destroys the value of bx!
    ; If bx must be preserved ; push    bx
    xor     bx, bx
    mov     cx, 5
    cld
decode_20_bit_addr_loop:
    lodsb
    ; Note: only space is accepted as valid "end" character
    ;   (but see "preprocessing")
    cmp     al, 20h
    jne     short decode_20_bit_addr_loop_1
    ; Exit with at most 4 digits read. Move si back. Set cl to 0.
decode_20_bit_addr_ok_1:
    dec     si
    xor     cl, cl
decode_20_bit_addr_ok_2:
    mov     ax, bx
    ; If bx must be preserved ; pop     bx
    clc
    retn
decode_20_bit_addr_loop_1:
    call    decode_hex_digit
    jc      short decode_20_bit_addr_error_2
    ; Store bh in ah. This will be used for setting cl
    ;    if the loop ended and there were 5 digits.
    mov     ah, bh
    shl     bx, 1
    shl     bx, 1
    shl     bx, 1
    shl     bx, 1
    ; Hack, only bl used, not entire 16 bit register
    or      bl, al
    loop    decode_20_bit_addr_loop
    ; End of loop, 5 digits read.
    ;   Fixme, cl is set based on the value saved in ah.
    mov     cl, ah
    and     cl, 0f0h
    ; Test byte at si. If it is not a
    ;   space, then the input is either too long or invalid.
    cmp     byte [si], 20h
    je      short decode_20_bit_addr_ok_2
decode_20_bit_addr_error_1:
    ; Exit, error. Return 0xffff and 0xff. Note: si not decremented
    ; If the number is valid, si will point to
    ;   the space after the number/address
    mov     al, 0ffh
    stc
decode_20_bit_addr_error_2:
    ; Note: decode_hex_digit sets al to 0xff on error
    cbw
    mov     cl, al
    ; If bx must be preserved ; pop     bx
    retn
    
skip_spaces:
    ; Small helper routine for patch file decoding. Start position in
    ;   register si, updates si (di is only modified temporarily).
    ; Sets CF if the end of block reached. es must be equal to ds!
    ; NOTE: assumes that the end position of the buffer is in dx,
    ;   re-calculates the value of cx using this info
    ;   value of cx is destroyed
    mov     cx, dx
    sub     cx, si
    xchg    si, di
    mov     al, 20h
    repe    scasb
    xchg    si, di
    ; ZF=1 means we have reached the end of the block
    ;   (note the terminator at the end of buffer!)
    jnz     short skip_spaces_1
    ; Note: Does not move back in this case!
    stc
    retn
skip_spaces_1:
    ; Move one position back
    dec     si
    ; not needed  ; inc     cx
    clc
    retn
    
read_file:
    ; Reads a file using int 21h DOS API. Registers used: ds: segment of both
    ;   the file name and the buffer, bx = offset of file name, cx = number
    ;   bytes to read, dx = address of buffer. Returns the number of bytes
    ;   read in ax. Destroys the value of bx. On error, the program 
    ;   terminates with an error message.
    ; int 21h, ah = 25h - open existing file ; al=00h -> read only
    ; Save dx in bx. This is needed because dx = read buffer was chosen
    ;   to avoid confusion (value of dx is preserved, unless the program
    ;   terminates on error)
    xchg    bx, dx
    mov     ax, 3d00h
    ; TODO: Value of cx is not modified, according to Ralph Brown's interrupt
    ;   list, "CL = attribute mask of files to look for (server call only)"
    ; However, neither DosBox, nor FreeDos kernel seems to use it
    int     21h
    ; Carry flag is set on error. (File not closed this case)
    jnc     short read_file_open_ok
read_file_error:
    mov     dx, str_error_io
    mov     bl, exit_code_error_io
    jmp     exit_error
read_file_open_ok:
    ; Read the data - int 21h, ah=3fh ; dx - the read buffer, saved in bx,
    ;   bx - the file handle (returned in ax if open was successful)
    mov     dx, bx
    mov     bx, ax
    mov     ax, 3f00h
    int     21h
    ; The file must be closed if it was opened successfully but read failed
    ; save ax, error is reported based on the flags after int 21h/ax=3f00 
    ;   Avoid duplication of "close file" (int 21h, ah=3eh)
    pushf
    push    ax
    ; Close the file handle - int 21h, ah=3fh
    mov     ah, 3eh
    int     21h
    pop     ax
    popf
    jc      short read_file_error
    retn

always_execute:
    ; Set to 1 if the /E switch is used (always execute, even
    ;   if checks before patching have failed.)
    db      0h
num_exec_checks:
    ; The "patches" buffer actually contains two types of entries:
    ;   "executable file checks" and patches. num_exec_checks is 
    ;   the number of the former type of entries.
    db      0h
buffer_patches_end:
    ; The "patch" buffer contains patch entries (this includes both "checks"
    ;   "and patches") and offsets for segments in the patches. The
    ;   "segment" information is put at the end of the buffer, with its
    ;   "end" pointer (=buffer_patches_end) decremented by 2 when 
    ;   a new "segment" indicator is detected in patch file.
    ;   A value of patch_data_block_end indicates that there are
    ;   no segments that need modification after we know the memory
    ;   address where the program has been loaded.
    dw      patch_data_block_end
patch_data_end:
    ; The end position of the patch data. The buffer has no terminator,
    ;   instead, the pointer (si) is compared to this end offset
    dw      0h
offs_last_patch:
    ; This "variable" is used for implementing the "segment" syntax
    ;   in the patches. Its value may be 0 (no segment indicator found yet
    ;   in patches) or an offset in patch_data_block.
    dw      0h
data_param_block:
    ; The parameter block that will be passed to int 21h/ax=4b00h
    ; See limitation below
    ; Segment of environment 0 = from caller program
    dw      0h
    ; Poiner to cmdline args - Both DosBox and FreeDos seem to copy
    ;   128 bytes unchanged from this addr to the  PSP
    ; Second word "reserved" for segment!
    dw      cmdline_args_data_pos, 0
    ; Pointers to two FCBs, see limitations below
    dw      data_fcb, 0h
    dw      data_fcb, 0h
param_block_ss_sp:
    ; ss:sp and cs:ip for the child process
    ; set by int 21h, ax = 4b01h
    dw      0h, 0h
param_block_cs_ip:
    dw      0h, 0h
data_fcb:
    ; LIMITATION: The program always passes two empty FCBs to
    ;   int 21h/ah=4bh, even if there are command line arguments
    ; Very old programs that depend on these FCBs will not work properly
    db      0
    times   11 db 20h
    times   4 db 0h
    
input_file_sign:
    ; Signature for input file. The signature must start at the first byte
    ;   of the file, no space or comment can precede it
    db      '!D16PATCH '
    ; The space is required at the end (but due to the preprocessing, it will
    ;    match newline, comments ,too) (I know, this is essentially a hack)
    ; input_file_sign_end: for length calculation
input_file_sign_end:
str_dos_ver_err:
    db      'Unsupported DOS version (3.0+ required).'
    db      0dh, 0ah, 24h
str_patch_failure:
    db      'Error, one of the provided checks failed, cannot patch program.'
    db      0dh, 0ah, 24h
str_error_args:
    db      'Command Line Args'
    db      24h
str_error_io:
    db      'File I/O'
    db      24h
str_error_file_format:
    db      'Patch file format'
    db      24h
str_error_dos_exec:
    db      'DOS Execute'
    db      24h    
str_msg_error:
    db      ' error!'
str_error_end:
    db      0dh, 0ah, 24h

data_area_start:

