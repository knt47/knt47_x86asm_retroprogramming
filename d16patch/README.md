# D16PATCH

D16PATCH is a 16-bit real mode "in memory" patching utility for DOS 3.X+. It works in the following way:

1. Reads the name og the executable file and the patches form the input file.
2. Loads the 16-bit DOS program to be patched into a memory using a DOS API function (int 21h, ah=4bh, al=01h, [See Ralf Brown's Interrupt List](https://www.delorie.com/djgpp/doc/rbinter/id/51/29.html)), but does not execute it.
3. Performs checks for the executable and applies the patches.
4. "Executes" the program by jumping to its start address.
5. The program unloads itself from memory, this is not a "terminate and stay resident" (TSR) program.

The idea is not new, similar (but larger and with game-specific features) tools have been developed for some DOS games: ([Link](https://moddingwiki.shikadi.net/wiki/CKPatch)). The main differences between D16PATCH and the other utilities are:

1. It is "universal" in the sense that it does not make any assumptions about the program that will be "patched". Checks for the specific programs can be provided in the patch file (not required, but recommended.)
2. D16PATCH is significantly smaller, written entirely in x86 assembly language and lacks features like reading a file and using that for patching.

## Input file syntax

A input (patch) file must contain the following data, separated by whitespace and/or comments:

- Patch file signature, always !D16PATCH. Must start at the first byte of the file, no whitespace/comment allowed before it.
- Name of executable file to patch
- Number of "checks" (one digit)
- Checks for the executable file
- The patches (Checks and patches use the same format)

\# is the comment character. All characters after it will be ignored until the end of the line (or, more precisely, until the next \\n (0x0a) byte. Input files using \n or \r\n as line terminator are both accepted.) 

File format example:

    !D16PATCH
    # Note: first line is the signature. No comment/whitespace may precede it
    # == Path to executable file ==
    TEST1.EXE
    # == Number of "tests" ===
    # To check if this is really the correct executable
    # Note: this is a single hex digit (0-F)
    2
    # === Checks for the executable files ===
    # The patching utility will test whether these byte sequences
    #   are found at the correct locations. 
    # Warning: "Checks" and patches have an identical format and the
    #   very simple syntax used by the patching tool does not prescribe
    #   any separator between them (But of course, comments may be used.)
    # Also note that the EXE file offsets are random, and not for an actual program
    # Some random machine code from the EXE
    717f B81300CD10
    # A string from the binary file 
    1e1dc 53616d70x6c65
    # === Patches ===
    # Replace a 2-byte instruction with NOPs
    49ea 9090
    # Modify a null-terminated string (if it fits)
    17add 4578616d706c652073747x696e672e00
    

Both "checks" and "patches" have two parts, the executable file offset and the check/patch data separated by whitespace. The executable file offset is a single hex number with at most 5 digits .(Note: In 16-bit real mode, 1 MB memory can be addressed which corresponds to 20 bits.) The patch data can be 1-16 bytes long (inclusive). No characters other than hex digits (including whitespace) are accepted inside the patches. The only exception is the $ sign, which indicates a segment relocation. It works very much like the "relocation table" in DOS MZ EXE programs. The two bytes after the $ sign are interpreted as a 16-bit segment, relative to the segment where the program is loaded and treated accordingly.

## Command line usage

It requires one command line argument, the name of the patch file:

    d16patch PATCHES.TXT
    
The program also accepts a /e or /E (always execute) switch. This switch must be added before the input file path.  If this switch is present, and one or more checks fail, the program will be executed without "patching".
    
## Limitations

- Supports only 16-bit DOS programs, and not 32-bit applications using a DOS extender.
- Compressed executable files must be uncompressed first using the appropriate tools.
- Patch buffer is currently limited to 2.5 KB, but can be increased by modifying the assembly code.
- One patch can be at most 16 bytes long.
- The size of patch file is limited to 48 KB and the temporary buffer for it must fit in the 64 KB segment where the D16PATCH program was loaded.

See the d16patch.asm file for more information.
    
## License

2-clause BSD license, see LICENSE.TXT.

## Build instructions

Current version can be built using FASM (but see the d16patch.asm source file for more details):

    fasm d16patch.asm D16PATCH.COM
