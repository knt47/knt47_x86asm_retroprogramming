# x86 assembly retroprogramming projects

This repository contains my experiments in 16-bit x86 retroprogramming, mostly in assembly language.

- **frcell88**: [A text mode implementation of the game FreeCell for IBM-compatible PCs with 8086/8088 or newer CPU.](/knt47/knt47_x86asm_retroprogramming/src/branch/main/frcell88)
- **fcel512**: [A simplified, 512-byte (boot sector sized) implementation of game FreeCell](/knt47/knt47_x86asm_retroprogramming/src/branch/main/fcell512)
- **memdump**: [A 16-bit, real mode memory view program](/knt47/knt47_x86asm_retroprogramming/src/branch/main/memdump)
- **d16patch**: [A 16-bit "in memory patching" utility for DOS](/knt47/knt47_x86asm_retroprogramming/src/branch/main/d16patch)
- **small**: [A collection of a few, small 16-bit utilities for DOS](/knt47/knt47_x86asm_retroprogramming/src/branch/main/small)
