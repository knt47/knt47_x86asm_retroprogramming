; SETDATE - set system date

; Copyright (c) 2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm setdate.asm SETDATE.COM

; SETDATE changes the system date using a DOS interrupt. It is mainly intended
;   for DOSBox as its "standard" version has no DATE command. 

; The date must be specified in an ISO 8601 format (yyyy-mm-dd). Usage example:
;   setdate 1999-12-25

exit_success equ 0
exit_failure equ 1
psp_offs_cmd_line_args_start equ 81h

org 100h
use16

prog_main:
    ; Check DOS version, If version = 1.X, al should remain 0
    ; int 21h, ah = 30h Get DOS version
    mov     ax, 3000h
    xor     bx, bx
    int     21h
    ; Versions >= 2.0 should be OK
    cmp     al, 2h
    jae     short dos_ver_ok
    ; int 21h, ah = 9h. write string
    mov     ah, 9h
    mov     dx, str_dos_ver_error
    int     21h
    ; Use int 20h to exit from program on DOS 1.x
    int     20h
dos_ver_ok:
    ; Check the program segment prefix for the arguments
    ;   arg #1 is the first year used in the calculation,
    ;   arg #2 (optional) is the last year (inclusive.)
    ; Note: this is a .COM program, assumes cs == ds == es
    cld
    mov     di, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [di-1h]
    ; No command line arguments
    jcxz    args_error_1
    ; skip spaces
    mov     al, 20h
    repe    scasb
    
    ; Note that cl = number the remaining bytes - 1
    cmp     cl, 9h
    jb      short args_error_1
    jz      short year_number_1
    ; If cl > 9 (ZF = 0), chexk if the following char is a space
    cmp     byte [di + 9h], ' '
    je      short year_number_1
args_error_1:
    mov     dx, str_error_cmd_line
    mov     bl, exit_failure
    jmp     program_exit
year_number_1:
    lea     si, [di - 1h]
    mov     cl, 4h
    ; Convert year
    call    convert_num_to_int
    ; Save year number into bp
    mov     bp, bx
    ; Check for '-' separator
    lodsb
    cmp     al, '-'
    jne     short args_error_1
    mov     cl, 2h
    ; Convert month
    call    convert_num_to_int
    ; Save month number into di
    mov     di, bx
    ; Check for '-' separator
    lodsb
    cmp     al, '-'
    jne     short args_error_1
    mov     cl, 2h
    ; Convert day
    call    convert_num_to_int
    ; Set system time
    mov     cx, bp
    mov     dl, bl
    mov     ax, di
    mov     dh, al
    mov     ah, 2bh
    int     21h
    mov     dx, str_success
    mov     bl, exit_success
    or      al, al
    jz      program_exit
    mov     dx, str_set_date_error
    inc     bx
program_exit:
    ; int 21h, ah = 9h - write string to stdout
    mov     ah, 9h
    int     21h
    mov     dx, str_end_newline
    int     21h
    mov     ah, 4ch
    mov     al, bl
    int     21h

convert_num_to_int:
    ; Note: on failure, the return offset is left in the stack
    ; Number is read from ds:si, return value in bx
    xor     bx, bx
convert_num_to_int_1:
    lodsb
    sub     al, '0'
    jb      short args_error_1
    cmp     al, 9h
    ja      short args_error_1
    ; ah will be always 0 after this (save one byte)
    cbw
    xchg    ax, bx
    mov     dx, 0ah
    mul     dx
    ; Note: no overflow check after mul (CF, OF)
    add     bx, ax
    loop    convert_num_to_int_1
    retn

str_success:
    db      'System date successfully set.'
    db      24h
str_set_date_error:
    db      'Failed to set date.'
    db      24h
str_error_cmd_line:
    db      'Invalid command line argument.'
    db      24h
str_dos_ver_error:
    ; This string continues in str_end_newline, so no
    ;   24h terminator char!
    db      'DOS 2+ required.'
str_end_newline:
    db      0dh, 0dh, 0ah, 24h
