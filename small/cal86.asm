; CAL86 - Simple Unix "cal"-like utility for DOS

; Copyright (c) 2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Simple Unix "cal"-like utility for DOS 1.X+ (?) and 8088/8086+ CPU
; The program supports only the Gregorian calendar.
; It also cannot print dates before 1753, see this Wikipedia
;   article for explanation:
; https://en.wikipedia.org/wiki/Cal_(command)#Quirks_(1752)

; Program can be buiilt using both FASM or NASM.
;   fasm cal86.asm CAL86.COM
;   nasm cal86.asm -o CAL86.COM -fbin

; Usage (mm = month, yyyy = year):
;   cal86 [mm] [yyyy]

; Offset of command line arguments in the PSP, not including the
;   first "size" byte
psp_offs_cmd_line_args_start equ 81h

; Minimum & maximun year allowed
year_num_min equ 1753
year_num_max equ 2999

; The memory address where the year as string is stored.
addr_year_str equ com_file_end
; The end of the same buffer
addr_year_str_end equ (addr_year_str + 4h)
; The memory address where the year number is stored.
addr_year equ addr_year_str_end
; A 24-byte temporary buffer
line_buf equ (addr_year + 2h)


org 100h
use16

prog_start:
    ; Note - No dos version check, should run on DOS 1.x (?)
    ; bp = -1 indicates that no month was set
    mov     bp, -1
    ; Check the program segment prefix for the arguments
    ; Valid format for arguments: either a 4-digit year number or
    ;     an 1 or 2 digit month number followed by the year 
    ; Note: this is a .COM program, assumes cs == ds == es
    cld
    mov     di, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [di - 1h]
    ; No command line arguments (TODO, jump size limit)
    or      cl, cl
    jnz     short arg_parse_l1
    jmp     single_month_calendar
arg_parse_l1:
    ; skip spaces
    mov     al, 20h
    repe    scasb
    ; move one byte back (the actual start of arg)
    ; bp is -1, so none byte smaller instruction
    lea     si, [bp + di]
    inc     cx
    ; At least 4 bytes must be left
    cmp     cx, 4h
    jae     short arg_parse_month_l1
args_error_l1:
    mov     dx, cmd_line_args_err
    mov     ah, 9h
    int     21h
args_error_l2:
    mov     dx, usage_str
    int     21h
    int     20h
arg_parse_month_l1:
    ; Check if there is a space after 1 or 2 chars.
    ; If yes, there is a month argument. al = 20 at this point
    mov     di, 1h
    cmp     [si + 1h], al
    je      short arg_parse_month_l2
    cmp     [si + 2h], al
    jne     short arg_parse_year_l1
    inc     di
arg_parse_month_l2:
    call    convert_num_to_int
    ; Range check month
    mov     bp, bx
    dec     bp
    jns     arg_parse_month_l3
print_range_chk_error_l1:
    mov     dx, str_out_of_range_month
    mov     cx, str_month_range 
print_range_chk_error_l2:
    mov     ah, 9h
    int     21h
    mov     dx, str_out_of_range
    int     21h
    mov     dx, cx
    int     21h
    jmp     short args_error_l2
arg_parse_month_l3:
    ; Note: month - 1 is stored in bp (0..11)
    cmp     bp, 0bh
    ja      short print_range_chk_error_l1
    ; Skip spaces afrer month
    ;; lea     di, [si - 1h]
    ;; inc     cx
    mov     di, si
    mov     al, 20h
    repe    scasb
    lea     si, [di - 1h]
    inc     cx
arg_parse_year_l1:
    ; TODO: currently, nothing is accepted after the year number, even spaces
    ;     (unless command.com removes the spaces, this is not done DOS 2.X)
    cmp     cx, 4h
    jne     short args_error_l1
    mov     di, addr_year_str
    movsw
    movsw
    mov     di, 4h
    sub     si, di
    call    convert_num_to_int
    cmp     bx, year_num_min
    jae     short arg_parse_year_l3
arg_parse_year_l2:
    mov     dx, str_out_of_range_year
    mov     cx, str_year_range
    jmp     short print_range_chk_error_l2
arg_parse_year_l3:
    cmp     bx, year_num_max
    ja      short arg_parse_year_l2
    mov     [addr_year], bx
    ; Print single month or entire year, based on the value of bp
    or      bp, bp
    ; Note: This is very close to the jump size limit
    js      short print_calendar_full_year
    ;; if longer jump needed:  ; jns     short single_month_calendar
    ;; if longer jump needed:  ; jmp     print_calendar_full_year
    
single_month_calendar:
    ; This is executed in two different cases:
    ; 1. No year and month was provided. Use the current year,
    ;      obtained using int 21h, ah = 2ah. In this case, bp is -1.
    ; 2. Both a year and a month were provided, we need to check
    ;      whether this is the current year/month, and if yes,
    ;      mark the current day (value in si).
    ; int 21h, ah = 2ah - Get system date
    mov     ah, 2ah
    int     21h
    ; Store day (dl -> bx, later si)
    mov     bl, dl
    xor     bh, bh
    ; Check if month - 1 has been set
    or      bp, bp
    jns     short year_month_set
    ; Store month (dh - 1 -> bp)
    mov     al, dh
    cbw
    mov     bp, ax
    dec     bp
    ; Year in cx. Note: No range check here, DOS
    ;   should return a value between 1980 - 2099.
    ; We do not have the year in string format, we need to
    ;   convert it.
    mov     ax, cx
    mov     [addr_year], ax  ; shorter instruction!
    mov     si, 10
    mov     cx, 3h  ; execute the loop 3 times    
    mov     di, addr_year_str_end - 1h
year_int21h_to_str_loop:
    xor     dx, dx
    div     si
    add     dl, '0'
    mov     [di], dl
    dec     di
    loop    year_int21h_to_str_loop
    ; The first digit ("thousands") in al
    add     al, '0'
    stosb
    jmp     short print_single_month_cal
year_month_set:
    ; Check if this is current year/month. If yes, mark current day
    ;     bx = day number or 0 if nothing should be marked
    cmp     [addr_year], cx
    jne     short year_month_set_l1
    mov     al, dh
    cbw
    dec     ax
    cmp     ax, bp
    je      short print_single_month_cal
year_month_set_l1:
    xor     bx, bx
print_single_month_cal:
    ; Store bp * 3 + month_names into si
    mov     si, bp
    shl     si, 1
    lea     si, [bp + si + month_names]
    xor     dx, dx
    call    print_month_year
    mov     dx, days_of_week
    mov     ah, 9h
    int     21h
    ; TODO, extra space chars before newline
    mov     dx, str_end_newline_single
    int     21h
    mov     si, bx
    call    calc_dayofweek
    mov     di, 6h
print_single_month_cal_l1:
    push    bx
    call    print_number_row
    pop     bx
    add     bl, 7h
    dec     di
    jnz     short print_single_month_cal_l1
    int     20h
    
print_calendar_full_year:
    xor     bp, bp
    mov     si, month_names
    call    print_all_3_months
    mov     dx, str_end_newline_single
    mov     ah, 9h
    int     21h
    call    print_all_3_months
    mov     dx, press_key_text
    mov     ah, 9h
    int     21h
    ; Wiit for a keypress
    mov     ah, 8h
    int     21h
    mov     dx, str_end_newlines
    mov     ah, 9h
    int     21h
    call    print_all_3_months
    mov     dx, str_end_newline_single
    mov     ah, 9h
    int     21h
    call    print_all_3_months
    mov     dx, str_end_newlines
    mov     ah, 9h
    int     21h
    int     20h
    
convert_num_to_int:
    ; Note: on failure, the return offset is left in the stack
    ; Number is read from ds:si, return value in bx
    ; prints at most "di" bytes
    xor     bx, bx
convert_num_to_int_1:
    lodsb
    sub     al, '0'
    jb      short args_error_l4
    cmp     al, 9h
    ja      short args_error_l4
    ; ah will be always 0 after this (save one byte)
    cbw
    xchg    ax, bx
    mov     dx, 0ah
    mul     dx
    ; Note: no overflow check after mul (CF, OF)
    add     bx, ax
    dec     di
    loopne  convert_num_to_int_1
    retn
    
args_error_l4:
    ; TODO, due to jump size limit
    jmp     args_error_l1
    
calc_dayofweek:
    ; word at addr_year = year
    ; bp = month - 1
    ; destroys the value of ax, cx, dx
    ; assumes cs = ds = ss !
    ; returns: bl: 1 - day of week 
    ;   (1 = Sunday, 0 = Monday, -5 = Saturday)
    ; bh: number of days in month
    mov     ax, [addr_year]
    xor     cx, cx
    cmp     bp, 2h
    ; Decrement ax if month is January or February
    sbb     ax, cx
    ; May need a segment prefix, but not in this program
    mov     cl, [bp + term_for_month_plus_1]
    add     cx, ax
    sar     ax, 1
    sar     ax, 1
    add     cx, ax
    mov     bx, 25
    cwd
    idiv    bx
    sub     cx, ax
    sar     ax, 1
    sar     ax, 1
    add     ax, cx
    mov     bl, 7
    cwd
    idiv    bx
    ; TODO: An additional division is performed if the month is February
    ;   and the year is divisible by 4. This should be avoided (?)
    mov     bl, 1h
    sub     bl, dl
    mov     bh, [bp + month_num_days]
    cmp     bp, 1h
    jne     short calc_dayofweek_l2
    mov     ax, [addr_year]
    ; Not divisible by 4 - not leap year
    test    al, 3h
    jnz     short calc_dayofweek_l2
    mov     cx, 100
    cwd
    idiv    cx
    ; Divisible by 4, but not divisible by 100 -> leap year
    or      dx, dx
    jnz     short calc_dayofweek_l1
    ; Years divisible by 100 but not bí 400 are not leap years
    test    al, 3h
    jnz     short calc_dayofweek_l2
calc_dayofweek_l1:
    mov     bh, 29
calc_dayofweek_l2:
    retn

clear_buf:
    ; Destroys the value of al, cx, di. DF must be cleared.
    cld
    mov     di, line_buf
    mov     al, ' '
    mov     cx, 17h
    rep     stosb
    ; DOS int 21h, ah = 9h (print str) terminator 
    mov     al, 24h
    stosb
    retn
    
print_number_row:
    ; bl = start value (signed), bh = limit
    ; si = 1..31 (inclusive): print newline chars at the end instead of
    ;     spaces, mark the day with this number with an asterisk.
    ; si = 0:  print newline chars at the end, no day marked
    ; si > 31 (unsigned): print spaces at end, no day is highlighted
    ; es must be equal to ds.
    push    di
    ;; push    bx
    call    clear_buf
    mov     di, line_buf
    mov     cl, 7h
    mov     dl, bl  ; save bl into dl
    ; TODO, this is a hack
    cmp     bl, bh
    jg      short print_row_l7
    mov     ax, 1h
    sub     al, bl
    jl      short print_row_l2
    mov     bl, 1h
    sub     cx, ax
    jle     short print_row_l7
    add     di, ax
    add     di, ax
    add     di, ax
print_row_l2:
    mov     al, bl
    cbw
    mov     dh, 0ah
    div     dh
    add     ax, 301fh
    jmp     short print_row_l4
print_row_l3:
    inc     bx
    cmp     ah, 3ah
    jne     short print_row_l6
    mov     ah, '0'
print_row_l4:
    cmp     al, 30h
    jae     short print_row_l5
    or      al, 10h
print_row_l5:
    inc     ax
print_row_l6:
    stosw
    inc     di
    inc     ah
    cmp     bl, bh
    loopne  print_row_l3
print_row_l7:
    cmp     si, 1fh
    ja      short print_row_l8
    mov     cx, si
    mov     di, line_buf
    mov     word [di + 15h], 0a0dh
    jcxz    print_row_l8
    mov     al, dl
    cbw
    mov     dx, ax
    add     ax, 7h
    cmp     si, ax 
    jge     short print_row_l8
    sub     cx, dx
    jl      short print_row_l8
    add     di, cx
    add     di, cx
    add     di, cx
    mov     word [di], 2a20h
print_row_l8:
    mov     dx, line_buf
    mov     ah, 9h
    int     21h
    ;; pop     bx
    pop     di
    retn
    
print_buf_contents:
    ; see comment about the meaning of "si" at print_number_row
    ; Should preserve bx, di, bp, but destroys all other registers
    ;     (except of course, ss, segments)
    push    di
    jmp     short print_row_l7

print_days_three_months:
    ; Destroys the value of all registers (except ss, segments).
    ;     bp in incremented by 3.
    call    calc_dayofweek
    inc     bp
    mov     si, bx
    ; TODO, optimize ??
    call    calc_dayofweek
    inc     bp
    mov     di, bx
    call    calc_dayofweek
    inc     bp
    xchg    bx, si
    push    bp
    mov     bp, 6h
print_days_three_months_l1:
    push    bx
    call    print_number_row
    mov     bx, di
    call    print_number_row
    push    si
    mov     bx, si
    ; Required for printing newline chars instead spaces
    ;   after the third month
    xor     si, si
    call    print_number_row
    pop     ax
    add     al, 7h
    mov     si, ax
    mov     ax, di
    add     al, 7h
    mov     di, ax
    pop     bx
    add     bl, 7h
    dec     bp
    jnz     short print_days_three_months_l1
    pop     bp
    retn
    
print_month_year:
    ; dx = 0 -> newline at end, dx = -1 -> spaces at end
    ; si = start address for 3-character month text
    ; Should preserve bx. All other registers except bp
    ;     (+sp, segments) destroyed
    ; si is incremented by 3.
    call    clear_buf
    sub     di, 12h
    movsw
    movsb
    push    si
    inc     di
    mov     si, addr_year_str
    movsw
    movsw
    mov     si, dx
    call    print_buf_contents
    pop     si
    retn
    
print_all_3_months:
    mov     dx, -1
    call    print_month_year
    mov     dx, -1
    call    print_month_year
    xor     dx, dx
    call    print_month_year
    mov     dx, days_of_week
    mov     ah, 9h
    int     21h
    int     21h
    int     21h
    ; TODO, extra space chars before newline
    mov     dx, str_end_newline_single
    int     21h
    push    si
    call    print_days_three_months
    pop     si
    retn
    
term_for_month_plus_1:
    ; Corresponds to "t" from
    ; https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week#Sakamoto's_methods
    ; but 1 added to all numbers.
    db      1, 4, 3, 6, 1, 4, 6, 2, 5, 7, 3, 5
month_num_days:
    db      31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
month_names:
    db      'JanFebMarAprMayJunJulAugSepOctNovDec'
days_of_week:
    db      'Su Mo Tu We Th Fr Sa   '
    db      24h
press_key_text:
    db      0dh, 0ah
    db      'Press a key...'
    db      24h
cmd_line_args_err:
    db      'CmdLine Args error.'
str_end_newline_single:
    db      0dh, 0ah, 24h
str_out_of_range:
    db      ': out of range '
    db      24h
str_out_of_range_month:
    db      'month'
    db      24h
str_out_of_range_year:
    db      'year'
    db      24h
str_month_range:
    db      '1-12'
    db      0dh, 0ah, 24h
str_year_range:
    db      '1753-2999'
    db      0dh, 0ah, 24h
usage_str:
    db      'Usage: cal86 [month] [year]'

str_end_newlines:
    db      0dh, 0ah, 0ah, 24h
com_file_end:
    
