; CECHO - Print text in text mode with specified foreground / background color

; Copyright (c) 2021-2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm cecho.asm CECHO.COM

; Usage example:
;   cecho.com 1e:This is a test.
; The 8-bit hex number before the colon is the text attribute. The first
;   hex digit is the background (1=blue), the second is the text
;   color (e=yellow) 
; Print text with color, but do not start a new line:
;   cecho.com n1e:This is a test.
; Using the ~xx (where xx are two hexadecimal digits) syntax, characters
;   from the current code page can also be specified in the input
;   as character codes.

; Offset of the first byte of command line argument
;    in the Program Segment Prefix
psp_offs_cmd_line_args_start equ 81h
; Character 7eh ('~'): The following two characters are interpreted as hex
;   digits, and the character with the same code as the two-digit hex number
;   is printed.
code_print_hex_encoded_char equ 7eh
; Default value for the number of lines of the text mode screen
scr_lines_default equ 19h
; "Default" text attribute, light gray on black. The same value is used on 
;   monochrome, too.
text_attr_default equ 7h
; Video memory segments for color & monochrome
text_mode_vid_mem_seg equ 0b800h
;; vid_mem_seg_mono equ 0b000h
vid_mem_seg_mono_highbyte equ 0b0h

; Variables stored after end_com_file

; text_attr: The text attribute parameter from command line. No default
;   value, stored one byte after the end of the data from the COM file
text_attr equ end_com_file
; print_text_start_offset: The offset in the PSP where the string
;   that will be printed starts
print_text_start_offset equ (text_attr + 1)

; Used by decode_hex_digit
hex_decode_error equ 0ffh

org 100h
use16

; NOTE 1 - According to my tests, old PC BIOSes behave the following way when
;   scrolling is required in int 10h, ah = 0eh: The attribute of the first
;   character in the last line is read, and the attribute of the "new line"
;   appearing on the bottom of the screen will use this attribute. (As if
;   the bh parameter of int 10h, ah=6h was set to this attribute.) (DOSBox 
;   behaves differently, it seems to use light gray on black, always.)
; NOTE 2 - This program calls int 10h, ah=6h so that it can specify the
;   default text attributes (07h) for the new lines after scrolling. The 
;   cursor position is set using int 10h, ah=2h. The characters are written
;   directly into the video memory.
; NOTE 3 - When the "n" (or "N" option is used to print only a part of a line,
;   and the rest is printed using e.g. the DOS API, the colors for the new 
;   lines after scrolling will be different than what this program does. For
;   example, if this program is used for printing red text, the text color
;   for the DOS prompt can also become red after scrolling. See NOTE 2 on
;   how the program avoids this if the "n" option is not used. See also
;   NOTE 1 about text mode scrolling in PC video BIOS.
; NOTE 4 - In DOS, ANSI.SYS provides similar functionality. This program
;    works even if this driver was not loaded (see NOTE 2 for implementation
;    details.)
; NOTE 5 - Except for the argument parse error message, output 
;   cannot be redirected using > in DOS command line

arg_parse_start:
    mov     si, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [si - 1h]
    ; Report an error if there are no command line arguments
    jcxz    arg_parse_error
    ; skip spaces and tabs
    cld
arg_parse_1:
    lodsb
    cmp     al, 20h  ; space
    je      short arg_parse_2
    cmp     al, 9h  ; tab
    jne     short arg_parse_3
arg_parse_2:
    loop    arg_parse_1
    ; Report an error if the loop terminates because cx reaches 0
arg_parse_error:
    ; Exit with error message
    mov     ah, 9h
    mov     dx, error_arg_parse
    int     21h
    ; int 21h, ah = 4ch -> exit to DOS
    mov     ax, 4c01h  ; exit code = 1 for error
    int     21h
    ; Hack fix for DOS 1.X where int 21h, ah = 4ch is not available
    ; int 20h -> exit with no exit code
    int     20h
arg_parse_3:
    ; check if there is at least 4 non-space chars left
    ;  e.g. 0e:<one_char_text> ; First non-space char in al. 
    cmp     cx, 4h
    jb      short arg_parse_error
    ; First char is 'N' or 'n' - no newline
    cmp     al, 'N'
    je      short arg_parse_no_newl
    cmp     al, 'n'
    jne     short arg_parse_get_attr
arg_parse_no_newl:
    ; Set "no newline at end" flag to 1
    inc     byte [no_newline_at_end]
    ; Decrease cx & check if there are another 4 characters left
    dec     cx
    cmp     cx, 4h
    jb      short arg_parse_error
    lodsb  ; load next byte
arg_parse_get_attr:
    ; Try to decode next char as hex digit
    call    decode_hex_digit
    ; sets carry flag on error
    jc      short arg_parse_error
    ; Put the attribute into register ah.
    mov     ah, al
    lodsb
    call    decode_hex_digit
    jc      short arg_parse_error
    ; Set the less significant nibble in ah ; Note: we cannot use shl bl, cl
    shl     ah, 1
    shl     ah, 1
    shl     ah, 1
    shl     ah, 1
    or      ah, al
    mov     [text_attr], ah
    ; There must be a ':' separator char between the attribute
    ;   and the text (better readability)
    lodsb
    cmp     al, ':'
    jne     short arg_parse_error
    ; Subtract 3 bytes ("AA:" where A is one hex digit of attribute) from cx
    sub     cx, 3h
    ; Check '~'s and the hex numbers following it.
    ; Save starting offset of the string
    mov     [print_text_start_offset], si
    ; bp will contain the number of actual characters to print
    xor     bp, bp
arg_parse_check_hex:
    lodsb
    cmp     al, code_print_hex_encoded_char
    jne     short arg_parse_4
    ; Check if check if there are another 3 characters left
    cmp     cx, 3h
    jb      short arg_parse_error
    lodsb   
    call    decode_hex_digit
    jc      short arg_parse_error
    lodsb   
    call    decode_hex_digit
    jc      short arg_parse_error
    dec     cx
    dec     cx
arg_parse_4:
    inc     bp
    loop    arg_parse_check_hex
    
    ; Check video adapter
check_video:
    xor     ax, ax
    mov     es, ax
    ; int 10h, ah=12h, bh=10h -> get EGA/VGA information
    mov     ah, 12h
    mov     bl, 10h
    int     10h
    ; dx is the video memory segment
    mov     dx, text_mode_vid_mem_seg
    cmp     bl, 10h
    je      short check_video_1
    ; Card is EGA/VGA compatible
    ; Store the number of lines on screen in bl
    ; Read byte 84h (0:484h) from BIOS memory area
    ;   Note: the number of lines - 1 is stored there
    mov     al, [es:484h]
    mov     [screen_lines_minus_1], al
    jmp     short check_video_3
check_video_1:
    ; Detect monochrome (MDA/Hercules)
    ;   check the first byte of color/monochrome port numner in BIOS data area
    ; No snow workaround is needed on mono (and the port numner in 
    ;   wait_for_vblanking is also invalid)
    cmp     byte [es:463h], 0b4h
    je      short check_video_2
    ; CGA card, set cga_snow_workaround
    inc     byte [cga_snow_workaround]
    jmp     short check_video_3
check_video_2:
    ; monochrome card, modify the video memory segment
    mov     dh, vid_mem_seg_mono_highbyte
check_video_3:
    ; Set es to the video memory segment
    mov     es, dx
    ; int 10h, ah = 3h -> Get text mode cursor position.
    ;   x coordinate in dl, y in dh
    mov     ah, 3h
    int     10h
    ; Get the current video mode (-> al), the current page (-> bh)
    ;   and the number of text mode screen columns (->ah)
    ; int 10h, ah = 0fh -> get video mode
    ; TODO: page number is ignored
    mov     ah, 0fh
    int     10h
    mov     cl, ah  ; Save ah into cl
    mov     ch, [screen_lines_minus_1]
    ; Calculate the "end" position for the text cursor
    mov     al, dh
    mul     ah
    xor     dh, dh
    add     ax, dx
    ; Save this value to di
    mov     di, ax
    add     ax, bp     
    ; New x, y position -> al, ah
    div     cl
    xchg    al, ah
    ; Update the x/y position if a newline must be printed
    ;   at the and
    cmp     byte [no_newline_at_end], 0h
    jne     short calc_pos_1
    inc     ah
    xor     al, al
calc_pos_1:
    ; Save its value into si
    mov     si, ax
    ; Check if we need to scroll screen
    mov     dh, ah
    sub     dh, ch
    jbe     skip_scroll
    xor     dl, dl
    ; Take scrolling into account in new cursor position
    sub     si, dx
    ; Take scrolling into account in the screen offset where the
    ;   text is written
    mov     al, dh
    mul     cl
    sub     di, ax
    ; Save the value of cx
    push    cx
    mov     al, dh
    mov     dx, cx
    ; Calculate bottom left corner, x coordinate must be decremented
    ;   (y (dh) already contains the number of lines - 1)
    dec     dx
    xor     cx, cx

    ; May destroy the value of bp on some BIOSes. Preserve cx, bx
    push    bx
    ; Rext attribute for the new rows on the screen
    mov     bh, text_attr_default
    ; int 10h, ah = 6h -> scroll window
    mov     ah, 6h
    ;; push    si
    push    bp
    int     10h
    pop     bp
    ;; pop     si
    pop     bx
    pop     cx
skip_scroll:
    ; int 10h, ah = 2h -> Set cursor position
    mov     dx, si
    mov     ah, 2h
    int     10h
    ; The position for the output on the screen
    shl     di, 1
    ; TODO, can only write to page 0
    ; One vertical blanking should be enough on CGA to print all
    ;   characters (?)
    call    wait_for_vbl
    ; Finally write the characters
    mov     ah, [text_attr]
    mov     si, [print_text_start_offset]
write_loop:
    lodsb
    cmp     al, code_print_hex_encoded_char
    jne     short write_loop_1
    ; Note: the carry flag form the conversion is ignored
    ;   as the input has already been validated.
    lodsb
    call    decode_hex_digit
    mov     ch, al
    mov     cl, 4h
    shl     ch, cl
    lodsb
    call    decode_hex_digit
    or      al, ch
write_loop_1:
    stosw
    dec     bp
    jnz     short write_loop
    
    ; Exit to DOS with exit code 0
    int     20h
  
decode_hex_digit:
    ; Decodes a hex digit in in register al. Returns 0xff and sets
    ;   CF on error. Case insensitive, accepts '0'-'9', 'A'-'F', 'a'-'f'.
    ; The carry flag will be set if al < 30h
    sub     al, 30h
    jc      short decode_hex_digit_error_2
    ; Is the (original value of) input <= '9' ?
    cmp     al, 9h
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    ja      short decode_hex_digit_a_f
decode_hex_digit_ok:
    clc
    ret
decode_hex_digit_a_f:
    ; Digits 'A' - 'F', 'a' - 'f'
    ; Hack to support upper case with 'and'
    sub     al, 11h
    ; The carry flag will be set if the original input is < 41h
    jc      short decode_hex_digit_error_2
    and     al, 0dfh
    add     al, 0ah
    ; Is the (original value of) input <= 'F' ?
    cmp     al, 0fh
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    jbe     short decode_hex_digit_ok
decode_hex_digit_error_1:
    stc
decode_hex_digit_error_2:
    mov     al, hex_decode_error
    ret
    
wait_for_vbl:
    ; Workaround for CGA snow. Waits for a VBL (vertical blanking.)
    ; TODO, this affects performance of the program significantly,
    ;   a more advanced strategy needs to be implemented later.
    ; Note: destroys the value of ax and dx
    cmp     byte [cga_snow_workaround], 0
    ; not needed on EGA and better
    je      short wait_for_vbl3
    ; push    ax
    ; push    dx
    mov     dx, 3dah
wait_for_vbl1:
    in      al, dx
    test    al, 8h
    jnz     short wait_for_vbl1
wait_for_vbl2:
    in      al, dx
    test    al, 8h
    jz      short wait_for_vbl2
wait_for_vbl3:
    ; pop     dx
    ; pop     ax
    ret

error_arg_parse:
    ; Error message printed when command line arguments are invalid.
    ; Note: In contrast to the "normal" output, redirection to 
    ;    e.g. NUL will work
    db      'CECHO: Invalid command line argument.'
    db      0dh, 0ah, 24h
    ; Variables
screen_lines_minus_1:
    ; Number of screen lines for CGA/Hercules, minus 1. If the BIOS does not
    ;   support int 10h, ah = 12h, bl = 10h (pre-EGA), use the fallback value
    ;   of 24 for the number of screen lines - 1.
    db      scr_lines_default - 1h
no_newline_at_end:
    ; 1 if the "no newline at end" option is used, 0 otherwise
    db      0h
cga_snow_workaround:
    ; Set to 1 if this is (probably) CGA and workaround or snow is needed
    db      0h
end_com_file:


