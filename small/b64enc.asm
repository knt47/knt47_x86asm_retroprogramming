; B64ENC - Small DOS 16-bit base64 encode tool.

; Copyright (c) 2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm b64enc.asm B64ENC.COM

; Small DOS 16-bit base64 encode tool. Reads data from and writes data
;     to standard output.
; Implemented using only one bit shifts, no and/or etc.
;     This may result in more efficient code for 8086/8088 where the
;     number of CPU cycles required by the shl/shr instructions depends
;     on the  number of shifts. (And it lacks "shl/shr with immediate 
;     value other than 1" instructions.)

; Limitation: The read buf size must be divisible by output_line_length * 3
;      (or output_line_length * 12 if output_line_length 
;       is not divisible by 4)

output_line_length      equ 76

buf_input_offs          equ com_file_end
read_buf_size           equ (output_line_length * 0c0h)
buf_encoded_offs        equ (buf_input_offs + read_buf_size)
sp_check                equ \
    (buf_encoded_offs + output_line_length * 100h + 400h)

use16
org 100h

prog_main:
    ; Check DOS version, at least DOS 2.y is required. In DOS 1.x, int 21h,
    ;     al = 30h is not available, it will leave al unchanged (0).
    ; int 21h, ah = 30h -> Get DOS version
    mov     ax, 3000h
    int     21h
    or      al, al
    jnz     short dos_ver_ok
    mov     ah, 9h
    mov     dx, str_err_dos_verion
    int     21h
    int     20h
dos_ver_ok:
    ; Note: If a .COM program is executed on DOS, and the available conventinal
    ;     memory is less than 64 K, the program will start but the value of sp 
    ;     will be set to a value lower than 0xfffe.
    cmp     sp, sp_check
    ja      short stack_ok
    mov     dx, str_err_memory
exit_error:
    mov     ah, 9h
    int     21h
    mov     dx, str_end_newline
    int     21h
    mov     ax, 4c01h
    int     21h
stack_ok:
    cld
file_read_loop:
    ; int 21h, ah = 3fh -> read file
    mov     ah, 3fh
    ; bx = 0 -> read from standard input
    xor     bx, bx
    mov     cx, read_buf_size
    mov     dx, buf_input_offs
    int     21h
    jnc     short file_read_loop_l1
file_read_loop_error:
    mov     dx, str_err_file_io
    jmp     exit_error
file_read_loop_l1:
    ; Push the number of bytes that have been read into the stack
    push    ax
    mov     cx, ax
    ; Exit if nothing was read
    jcxz    program_end
    mov     bx, dx
    mov     di, buf_encoded_offs
    mov     si, b64_chars
    call    b64_encode
    mov     si, buf_encoded_offs
    mov     bx, 1h  ; standard output handle
output_loop:
    mov     cx, output_line_length
    mov     ax, di
    sub     ax, si
    cmp     ax, cx
    jae     short output_loop_l1
    mov     cx, ax
output_loop_l1:
    mov     dx, si
    ; int 21h, ah = 40h -> write to file
    mov     ah, 40h
    int     21h
    jc      short file_read_loop_error
    ; TODO, check the number of bytes actually written
    ;; cmp     ax, cx
    ;; jne     short file_read_loop_error
    add     si, cx
    mov     cx, 2h
    mov     dx, newline_chars
    ; int 21h, ah = 40h -> write to file
    mov     ah, 40h
    int     21h
    jc      short file_read_loop_error
    cmp     si, di
    jb      short output_loop
    pop     ax
    cmp     ax, read_buf_size
    je      short file_read_loop
program_end:
    int     20h
    
b64_encode:
    ; Simple base64 encode routine.
    ; ds:bx = the data buffer to encode
    ; es:di = output buffer
    ; ds:si = the base64 character array. (may be changed to cs:si,
    ;     see comments)
    ; cx = size of input
    ; Note destroys the value of all registers except si, sp, and segments
    ;     This also includes bp.
    ; At exit, di will point to the byte after the end of output buffer.
    ; DF must be 0
    ; Loosely based on:
    ; https://en.wikibooks.org/wiki/Algorithm_implementation/Miscellaneous/Base64
    
    ;; push    bp  ; Uncomment this if needed 
    ; Note: this is close to the jump size limit!
    jcxz    b64_encode_l4
b64_encode_loop1:
    lea     bp, [bx + 3h]
    xor     ax, ax
    xor     dh, dh
    mov     dl, [bx]
    inc     bx
    dec     cx
    jz      short b64_encode_l1
    mov     ah, [bx]
    inc     bx
    dec     cx
    jz      short b64_encode_l1
    mov     al, [bx]
    inc     bx
    ; cx intentionally not decremented here!
    ;; dec     cx
b64_encode_l1:
    sub     bx, bp
    ; dh <- dl(2..7)
    ; dl <- dl(0..1) << 4 + ah(4..7)
    ; ah <- ah(0..3) << 2 + al(6..7)
    ; al <- al(0..5)
    shl     ax, 1
    rcl     dx, 1
    shl     ax, 1
    rcl     dx, 1
    shl     ax, 1
    rcl     dx, 1
    shl     ax, 1
    rcl     dx, 1
    shl     dx, 1
    shl     dx, 1
    shr     dl, 1
    shr     dl, 1
    shr     ax, 1
    shr     ax, 1
    shr     al, 1
    shr     al, 1
    xor     bh, bh
    sar     bl, 1
    ; The value of the flags after sar, depending on the value of bl
    ;     set by "sub bx, bp":
    ;     bx = 0 -> ZF = 1, CF = 0, OF = 0
    ;     bx = -1 -> ZF = 0, CF = 1, OF = 0
    ;     bx = -2 -> ZF = 0, CF = 0, OF = 0
    ; Also note that none of the instructions until the end of loop
    ;     affects flags
    ; Save al into dh, it contains the last 6-bit value to write,
    ;     yet, the value of al will be destroyed.
    xchg    al, dh
    mov     bl, al
    ; Note: cs: prefix not needed
    ;; mov     al, [cs:bx + si]
    mov     al, [bx + si]
    stosb
    mov     bl, dl
    ;; mov     al, [cs:bx + si]
    mov     al, [bx + si]
    stosb
    ; Note: ja => jump if ZF = 0 and CF = 0
    ja      short b64_encode_l2
    mov     bl, ah
    ;; mov     al, [cs:bx + si]
    mov     al, [bx + si]
    stosb
    jc      short b64_encode_l3
    mov     bl, dh
    ;; mov     al, [cs:bx + si]
    mov     al, [bx + si]
    stosb
    mov     bx, bp
    loop    b64_encode_loop1
    jmp     short b64_encode_l4
b64_encode_l2:
    mov     al, '='
    stosb
b64_encode_l3:
    mov     al, '='
    stosb    
b64_encode_l4:
    ;; pop     bp  ; Uncomment this if needed 
    retn
    
b64_chars:
    db      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    
str_err_memory:
    db      'Not enough conventional memory.'
    db      24h
str_err_file_io:
    db      'File I/O error.'
    db      24h
str_err_dos_verion:
    db      'DOS 2+ required.'
str_end_newline:
    db      0dh
newline_chars:
    db      0dh, 0ah
    db      24h
com_file_end:
