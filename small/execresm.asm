; EXECRESM - Execute a DOS program with part of
;   conventional memory "reserved"

; Copyright (c) 2021-2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm execresm.asm EXECRESM.COM

; Usage example: execresm 128 TEST.EXE
; The first argument is the amount of memory to reserve in Kbytes*,
;   the second is the path to the program to execute.

; * Difference from EXECRESP

; Execute a DOS program with part of conventional memory "reserved"

; Offset of command line arguments in the PSP
psp_offs_cmd_line_args equ 80h
; The position after the size byte
psp_offs_cmd_line_args_start equ 81h
; The end of the same block
psp_offs_cmd_line_args_end equ 100h
; The offset of "process environment block" in PSP
psp_offs_proc_env_block equ 2ch

; the new location of the stack - hardcoded
new_stack_offs equ 7feh

; Offsets for arg parsing
offs_data_exec_name equ data_exec_name
; Name of executable limited to 63 bytes!
exec_name_size equ 80h
; Offset of the "args" buffer
offs_data_exec_args equ offs_data_exec_name + exec_name_size
; Number of bytes reserved for a command line arguments block (for exec)
exec_args_data_size equ 80h
; the end of the same memory block
offs_data_exec_args_end equ offs_data_exec_args + exec_args_data_size
; todo, add error codes
exit_success equ 0
exit_error equ 1

; Note - decimal constants!
reserve_kbytes_min equ 2
reserve_kbytes_max equ 640

; The program takes into account the memory occupied by the DOS
;   process environment block. If this block is located at most
;   this number of paragraphs below the program's code segment,
;   the difference will be subtracted from the size of memory block.
;   occupied by this program.
; (Default: 1 KB) Note: the program will not work correctly if
;   the value of this is above reserve_kbytes_min KB in paragraphs
env_block_num_paragr_check equ 40h
size_check_paragrpahs_min equ 80h

org 100h
use16  

prog_main:
    ; Re-locate stack to save memory. Note: The memory is only freed later,
    ;   so that we can use the memory block following the stack.
    ;  (It is assumed that at least 64K of is available for the program.)
    mov     sp, new_stack_offs
    ; Check DOS version, DOS exec may not restore ss, sp in older versions
    ; int 21h, ah = 30h Get DOS version
    mov     ax, 3000h
    xor     bx, bx
    int     21h
    ; Versions >= 2.0 should be OK
    cmp     al, 2h
    jae     short ver_ok
ver_error:
    mov     dx, str_err_dos_ver
    mov     ah, 9h
    int     21h
    int     20h
ver_ok:

    ; Now, check the program segment prefix for the arguments
    ;   arg #1 is the input file name, everything else will be passed
    ;   to the program. The file name will be copied to
    ;   the memory area starting at patch_file_name_temp_buf
    ; Note: this is a com program, assumes cs == ds == es
    cld
    mov     di, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [di - 1h]
    ; Report an error if there are no command line arguments
    jcxz    arg_parse_error
    ; skip spaces
    mov     al, 20h
    repe    scasb
    ; move one byte back (the actual start of arg)
    dec     di
    inc     cx
    
    ; NEW - Process kilobytes to reserve
    xor     bx, bx
mem_reserve_kb_1:
    xor     ah, ah
    mov     al, [es:di]
    ; Space - end (Note that this should not occur in
    ;   first execution of loop)
    cmp     al, 20h
    je      short mem_reserve_kb_2
    cmp     al, '0'
    jb      short arg_parse_error
    cmp     al, '9'
    ja      short arg_parse_error
    sub     al, '0'
    xchg    ax, bx
    mov     dx, 0ah
    mul     dx
    ; mul sets CF and OF if upper 16 bits of result is not 0
    jc      short arg_parse_error
    add     bx, ax
    jc      short arg_parse_error
    cmp     bx, reserve_kbytes_max
    ja      short arg_reserve_kb_error
    inc     di
    ; Decrement cx !
    loop    mem_reserve_kb_1
arg_parse_error:
    ; no arguments provided, error
    mov     dx, str_err_cmdline_args
    mov     bl, exit_error
    jmp     prog_exit
mem_reserve_kb_2:
    cmp     bx, reserve_kbytes_min
    jae     mem_reserve_kb_3
arg_reserve_kb_error:
    mov     dx, str_err_arg_reserve
    mov     bl, exit_error
    jmp     prog_exit
mem_reserve_kb_3:
    ; multiply the value of bx by 64 (kbytes->paragraphs)
    ; save cx into ax
    mov     ax, cx 
    mov     cl, 6h
    shl     bx, cl
    mov     cx, ax
    
    ; The program takes into account the memory occupied by the DOS
    ;   process environment block. If this block is located at most
    ;   this number of paragraphs below the program's code segment,
    ;   the difference will be subtracted from the size of memory block.
    ;   occupied by this program.
    mov     ax, cs
    sub     ax, [psp_offs_proc_env_block]
    ; Process env. block is above code segment (??)
    jc      short mem_reserve_kb_end
    cmp     ax, env_block_num_paragr_check
    ja      short mem_reserve_kb_end
    sub     bx, ax
    ; size_check_paragrpahs_min paragraphs must remain
    ;   due to stack size
    mov     dx, size_check_paragrpahs_min
    cmp     bx, dx
    jae     short mem_reserve_kb_end
    mov     bx, dx
mem_reserve_kb_end:

    ; skip spaces
    mov     al, 20h
    repe    scasb
    ; move one byte back (the actual start of arg)
    dec     di
    inc     cx
    ; save the position in si 
    mov     si, di
    ; find the end of the argument
    mov     al, 20h
    repne   scasb

    ; Check if a space was found
    jnz     short arg_parse_1
    ; move one byte backwards but only if a space was found
    dec     di
arg_parse_1:
    ; set the number of bytes to be copied
    mov     cx, di
    sub     cx, si
    ; report an error if the number of bytes is zero
    jz      short arg_parse_error

arg_parse_copy:
    ; Copy the file name to offs_data_exec_name
    mov     di, offs_data_exec_name
    ; mov     bx, di
    rep     movsb
    ; Add a null terminator
    xor     al, al
    stosb
copy_cmdline_args:
    ; Copy finished, now fill the (offs_)data_exec_args block
    ; Calculate the new length and the number of bytes to copy
    ; New_length(ax) = Old_length - (index(si) - Block_start_81h)
    ; Bytes_to_copy(cx) = Block_end - index(si)
    xor     ah, ah
    mov     al, [psp_offs_cmd_line_args]
    add     ax, psp_offs_cmd_line_args_start
    sub     ax, si
    mov     cx, psp_offs_cmd_line_args_end
    sub     cx, si
    mov     di, offs_data_exec_args
    ; store the size - al
    stosb
    ; copy the remaining bytes in the cmd line args part of PSP
    rep     movsb
    ; fill the remaining bytes of (offs_)data_exec_args with 0
    mov     cx, offs_data_exec_args_end
    sub     cx, di
    xor     al, al
    rep     stosb
    
    
mem_resize:
    ; Free the memory after the stack
    ; int 21h, ah = 4ah - resize memory block
    ; Note: es has already been restored
    mov     ah, 4ah
    int     21h
    jnc     short mem_resize_ok
    ; Carry flag set on error
    ; Note: reports "DOS Exec" error if memory deallocation fails
    mov     dx, str_err_dos_exec
    mov     bl, exit_error
    jmp     prog_exit
mem_resize_ok:
    
   ; Execute the program. Restore es first (also store cs in ax)
    mov     ax, cs
    mov     es, ax
    ; set segments in "parameters block" (todo: "magic" numbers)
    mov     [data_param_block + 4h], ax
    mov     [data_param_block + 8h], ax
    mov     [data_param_block + 0ch], ax
    ; int 21h, ax = 4b00h - execute program
    ;   Note: segments es: (param block) ds: (exe path)
    ; LIMITATION: The program always passes two empty FCBs to
    ;   int 21h/ax=4b00h, even if there are command line arguments
    ;   See further details at data_fcb
    mov     ax, 4b00h
    mov     dx, offs_data_exec_name
    mov     bx, data_param_block
    int     21h
    ; Note: restores ss, sp and ds. Note: sp is simply set to the value
    ;   used at program start
    cli
    mov     dx, cs
    mov     ss, dx
    mov     ds, dx
    mov     sp, new_stack_offs
    sti
    mov     dx, str_exec_ok
    mov     bl, exit_success
    ; Carry flag set by int 21h, cli/sti/mov does not affect it
    jnc     short prog_exit
    ; TODO, check error code in al
    mov     dx, str_err_dos_exec
    mov     bl, exit_error
prog_exit:
    ; error code in bl!
    mov     ah, 9h
    int     21h
    mov     dx, print_newl
    int     21h
    mov     ah, 4ch
    mov     al, bl
    int     21h
    
data_param_block:
    ; The parameter block that will be passed to int 21h/ax=4b00h
    ; See limitation below
    ; Segment of environment 0 = from caller program
    dw      0h
    ; Pointer to cmdline args - Both DosBox and FreeDos seem to copy
    ;   128 bytes unchanged from this addr to the  PSP
    ; Second word "reserved" for segment!
    dw      offs_data_exec_args, 0
    ; Pointers to two FCBs, see limitations below
    dw      data_fcb, 0
    dw      data_fcb, 0
data_fcb:
    ; LIMITATION: The program always passes two empty FCBs to
    ;   int 21h/ax=4b00h, even if there are command line arguments
    db      0h
    times   11 db 20h
    times   4 db 0h
    
str_err_cmdline_args:
    db      'Command Line Args Error.'
    db      24h
str_err_arg_reserve:
    db      'Reserved memory must be between 2 KB and 640 KB.'
    db      24h
str_err_dos_exec:
    db      'DOS exec failure.'
    db      24h
str_exec_ok:
    db      'Program execution successful.'
    db      24h
str_err_dos_ver:
    db      'Requires at least DOS 2.0.'
    ; Note: Intentionally no "db 24h" (terminator) here)
print_newl:
    db      0dh, 0ah, 24h
    
data_exec_name:
    
