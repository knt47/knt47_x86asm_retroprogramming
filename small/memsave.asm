; MEMSAVE - save a real mode memory dump to a binary file

; Copyright (c) 2021-2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm memsave.asm MEMSAVE.COM

; Usage example: memsave c000+0800 outfile.bin
; (c000 is the start segment, 0800 is the number of paragraphs to save)

; The position after the size byte in the PSP
psp_offs_cmd_line_args_start    equ 81h
; Maximum number of paragraphs to write to the output file at once.
;   Must be a power of 2.
write_max_num_paragr            equ 800h
; Maximum number of bytes to write to the output file at once
write_max_num_bytes             equ (write_max_num_paragr * 10h)
; Bit shift corresponding to write_max_num_paragr
write_max_num_paragr_bitshift   equ 0bh
; Used by decode_hex_digit
hex_decode_error                equ 0ffh
; Exit codes
exit_success                    equ 0
exit_failure                    equ 1

; For NASM compatibilty, comment out if using NASM
strict equ

org 100h
use16

prog_main:
    mov     ax, 3000h
    xor     bx, bx
    int     21h
    ; Versions >= 2.0 should be OK
    cmp     al, 2h
    jae     short ver_ok
ver_error:
    mov     dx, str_err_dos_ver
    mov     ah, 9h
    int     21h
    int     20h
ver_ok:

parse_args:
    cld
    mov     di, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [di-1h]
    ; No command line arguments
    jcxz    args_error_1
    ; skip spaces
    mov     al, 20h
    repe    scasb
    
    ; Note that cl = number the remaining bytes - 1
    cmp     cl, 9h
    jb      short args_error_1
    ; chexk if the following char is a space
    cmp     byte [di + 8h], ' '
    je      short parse_args_segment
args_error_1:
    mov     dx, str_err_cmd_line
    mov     bl, exit_failure
    jmp     program_exit
parse_args_segment:
    lea     si, [di - 1h]
    mov     dx, cx  ; save cx into dx
    call    decode_4_digit_hex_number
    jc      short args_error_1
    ; Save first number (start segment) into bp
    mov     bp, bx
    ; Separator between the two numbers must be '+'
    lodsb
    cmp     al, '+'
    jne     short args_error_1
    call    decode_4_digit_hex_number
    jc      short args_error_1
    ; restore the value of cx, si
    mov     cx, -8h
    add     cx, dx
    ; skip spaces
    mov     di, si
    mov     al, 20h
    repe    scasb
    ; move one byte back (the actual start of arg)
    dec     di
    inc     cx
    
    ; save the position in si 
    mov     si, di
    ; find the end of the argument
    mov     al, 20h
    repne   scasb
    ; Check if a space was found
    jnz     short parse_args_l1
    ; move one byte backwards but only if a space was found
    dec     di
parse_args_l1:
    ; set the number of bytes to be copied
    mov     cx, di
    sub     cx, si
    ; report an error if the number of bytes is zero
    jz      short args_error_1
parse_args_copy:
    ; Copy the file name to out_file_name
    mov     di, out_file_name
    rep     movsb
    ; Add a null terminator at the end
    mov     byte [di], 0h
    mov     si, bx  ; Save the value of bx into si
    ; int 21h, ah=3ch -> create or truncate file
    mov     ah, 3ch
    xor     cx, cx
    mov     dx, out_file_name
    int     21h
    jc      short file_io_error_l2
    mov     bx, ax  ; file handle = bx
    ; Replace the terminator of the file name with a '$', offset still in di
    mov     byte [di], '$'
    ; Calculate the number of write_max_num_bytes blocks to save, put
    ;   the result in di and the "remainder" into si
    ; Note: this assumes that write_max_num_paragr is a power of 2
    mov     di, si
    mov     cl, write_max_num_paragr_bitshift
    shr     di, cl
    and     si, strict word write_max_num_paragr - 1h
    mov     cl, 4h
    shl     si, cl  ; convert paragraphs to bytes
    mov     cx, write_max_num_bytes  ; write buffer size at start
write_data_l1:
    ; Note: di will be decremented to -1 and also to -2 if the value of si
    ;   was nonzero before start of loop. Maximum possible value of 
    ;   di is 1fh, so it is not a problem to treat di as "signed"
    dec     di
    jns     short write_data_l2
    ; Remaining bytes for last write, skip if zero
    mov     cx, si
    jcxz    write_data_l3
    xor     si, si
write_data_l2:
    ; data offset for int 21h, ah = 40h => write file
    xor     dx, dx
    mov     ds, bp
    mov     ah, 40h
    int     21h
    jc      short file_io_error_l1
    ; Fewer bytes written than needed?
    cmp     ax, cx
    jne     short file_io_error_l1
    ; Advance bp, which stares the starting segment for the write
    add     bp, write_max_num_paragr
    jmp     short write_data_l1
file_io_error_l1:
    ; Note that this is part of code is unreachable from the previous loop 
    ;   if no error jappened
    ; int 21h, ah = 3eh -> Close the file, todo, this is duplicated
    mov     ah, 3eh
    int     21h
file_io_error_l2:
    mov     dx, str_err_dos_io
    mov     bl, exit_failure
    jmp     program_exit
write_data_l3:
    ; int 21h, ah = 3eh -> Close the file, todo, this is duplicated
    mov     ah, 3eh
    int     21h
    mov     dx, str_file_saved
    mov     bl, exit_success
program_exit:
    mov     ax, cs
    mov     ds, ax
    mov     ah, 9h
    int     21h
    mov     dx, print_newl
    int     21h
    mov     al, bl  ; bl - error code 
    mov     ah, 4ch
    int     21h
    
decode_hex_digit:
    ; Decodes a hex digit in in register al. Returns 0xff and sets
    ;   CF on error. Case insensitive, accepts '0'-'9', 'A'-'F', 'a'-'f'.
    ; The carry flag will be set if al < 30h
    sub     al, 30h
    jc      short decode_hex_digit_error_2
    ; Is the (original value of) input <= '9' ?
    cmp     al, 9h
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    ja      short decode_hex_digit_a_f
decode_hex_digit_ok:
    clc
    retn
decode_hex_digit_a_f:
    ; Digits 'A' - 'F', 'a' - 'f'
    ; Hack to support upper case with 'and'
    sub     al, 11h
    ; The carry flag will be set if the original input is < 41h
    jc      short decode_hex_digit_error_2
    and     al, 0dfh
    add     al, 0ah
    ; Is the (original value of) input <= 'F' ?
    cmp     al, 0fh
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    jbe     short decode_hex_digit_ok
decode_hex_digit_error_1:
    stc
decode_hex_digit_error_2:
    mov     al, hex_decode_error
    retn
    
decode_4_digit_hex_number:
    ; Result in register bx. Destroys the value of ax, cx. "Advances" si.
    ;   Sets the carry flag on error.
    mov     cx, 404h
    xor     bx, bx
decode_4_digit_hex_l1:
    lodsb
    call    decode_hex_digit
    jc      short decode_4_digit_hex_l2
    shl     bx, cl
    or      bl, al
    dec     ch
    jnz     short decode_4_digit_hex_l1
    ; The next instruction is not necessary, because the last instruction
    ;   that modified CF was "or", which always clears it
    clc
decode_4_digit_hex_l2:
    retn

str_err_dos_ver:
    db      'Requires at least DOS 2.0.'
print_newl:
    db      0dh, 0dh, 0ah, 24h
str_err_cmd_line:
    db      'Command Line Args Error.'
    db      24h
str_err_dos_io:
    db      'DOS I/O Error.'
    db      24h
str_file_saved:
    db      'Dump saved as '
out_file_name:

