; EASTER86 - Calculates the date of Easter for one year or
;   a date range (first year..last year) 

; Copyright (c) 2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm easter86.asm EASTER86.COM

; Calculates the date of Easter for one year or
;   a date range (first year last year) (according to the
;   Gregorian calendar).

; Offset of command line arguments in the PSP, not including the
;   first "size" byte
psp_offs_cmd_line_args_start equ 81h
; Minimum & maximum year allowed
; LIMITATION in current version - Year number must be 4 digits long.
year_num_min equ 1000
year_num_max equ 9999

org 100h
use16  

prog_main:
    ; Note - No dos version check, should run on DOS 1.x (?)

    ; Check the program segment prefix for the arguments
    ;   arg #1 is the first year used in the calculation,
    ;   arg #2 (optional) is the last year (inclusive.)
    ; Note: this is a .COM program, assumes cs == ds == es
    cld
    mov     di, psp_offs_cmd_line_args_start
    xor     ch, ch
    ; Note: es:/cs: prefix not required
    mov     cl, [di - 1h]
    ; No command line arguments
    jcxz    no_year_provided
    ; skip spaces
    mov     al, 20h
    repe    scasb
    ; move one byte back (the actual start of arg)
    dec     di
    inc     cx
    call    year_number_to_int
    ; Copy the 4 characters long year number. Save di to dx.
    mov     dx, di
    lea     si, [di - 4h]
    mov     di, str_print_year_number_end - 4h
    ; Copy exactly 4 bytes
    movsw
    movsw
    ; Store first year in si (value in ax preserved!)
    mov     si, ax
    mov     di, dx  ; Restore di
    jcxz    arg_parse_l2
    ; skip spaces
    mov     al, 20h
    repe    scasb
    mov     ax, si  ; for 2 bytes shorter code
    dec     di
    inc     cx
    ; Convert the second argument to integer
    ; TODO, will complain about extra space(s) at the end of command line
    ;   (because bx will be 0 which is below the minimum allowed value)
    call    year_number_to_int
arg_parse_l2:
    ; di = first year (previously in si) ; si = last year
    mov     di, si
    mov     si, ax
    jmp     print_date_loop

no_year_provided:
    ; No year was provided. Use the current year,
    ;   obtained using int 21h, ah = 2ah.
    ; int 21h, ah = 2ah - Get system date
    mov     ah, 2ah
    int     21h
    ; Year in cx. Note: No range check here, DOS
    ;   should return a value between 1980 - 2099.
    ; We do not have the year in string format, we need to
    ;   convert it.
    mov     si, cx  ; save year into si
    mov     ax, cx
    mov     bx, 10
    mov     cx, 3h  ; execute the loop 3 times    
    mov     di, str_print_year_number_end - 1h
year_int21h_to_str_loop:
    xor     dx, dx
    div     bx
    add     dl, '0'
    mov     [di], dl
    dec     di
    loop    year_int21h_to_str_loop
    ; The first digit ("thousands") in al
    add     al, '0'
    stosb
    ; Single year, si and di must be the same
    mov     di, si
    
print_date_loop:
    call    calc_easter_date
    mov     bx, str_print_date_march
    cmp     ax, 31
    jle     short print_date_l1
    sub     ax, 31
    mov     bx, str_print_date_april
print_date_l1:
    mov     dl, 10
    div     dl
    add     ax, '00'
    mov     [bx], ax
    ; int 21h, ah = 9h - write string
    mov     ah, 9h
    mov     dx, bx
    int     21h
    ; To avoid extra divisions (slow on 8088),
    ;    increment the decimal year number separately. 
    mov     bx, str_print_year_number_end - 1h
    mov     dx, str_print_year
    ; int 21h, ah = 9h - write string
    int     21h
    inc     dx  ; Starting space!
print_date_l2:
    inc     byte [bx]
    cmp     byte [bx], 3ah
    jb      short print_date_l3
    mov     byte [bx], '0'
    dec     bx
    cmp     bx, dx
    jae     short print_date_l2
print_date_l3:
    inc     di
    cmp     di, si
    jle     short print_date_loop
    ; Exit. Dos 1.X compatible, but no return code
    int     20h
    
calc_easter_date:
    ; == Manual conversion of the following code to x86-16 assembly: == 
    ; (1)  K = INT (X/100); 
    ; (2)  M = 15 + INT ((3K+3)/4) - INT ((8K+13)/25); 
    ; (3)  S = 2 - INT ((3K+3)/4); 
    ; (4)  A = MOD (X, 19); 
    ; (5)  D = MOD (19A+M, 30); 
    ; (6)  R = INT (D/29) + (INT (D/28) - INT (D/ 29)) * INT (A/11); 
    ; (7)  OG = 21 + D - R;
    ; (8)  SZ = 7 - MOD (X+INT (X/4)+S, 7);
    ; (9)  OE = 7 - MOD (OG-SZ, 7);
    ; (*)  OS = OG + OE
    ; Source: https://www.ptb.de/cms/ptb/fachabteilungen/abt4/fb-44/ag-441/darstellung-der-gesetzlichen-zeit/wann-ist-ostern.html
    ; Note that (6) is equivalent to R = IF(D == 29 ; 1 ; 0) + 
    ;     + IF(D == 28; IF(A >= 11 ; 1 ; 0) ; 0)
    ;   if 0 <= A < 19 and 0 <= D < 30.
    ; Contains some optimizations. TODO: better optimized code
    ; -- Year in di, return value in ax. --
    ; -- Destroys the value of bx, cx, dx. Preserves si, di. --
    push    si
    mov     ax, di
    cwd
    mov     cx, 100
    idiv    cx
    mov     bx, ax
    shl     bx, 1
    add     bx, ax
    mov     cl, 3
    shl     ax, cl
    add     bx, cx
    dec     cx
    sar     bx, cl
    neg     bx
    add     ax, 13
    mov     cl, 25
    cwd
    idiv    cx
    mov     si, ax
    ; -- di = X (Year) ; bx = -(3 * K + 3) / 4 ; --
    ; -- si = (8 * K + 13) / 25 --
    mov     cl, 19
    mov     ax, di
    cwd
    idiv    cx
    mov     ax, dx
    mov     dx, cx
    lea     cx, [bx + si - 15]
    mov     si, ax
    ; -- cx = (8 * K + 13) / 25 - (3 * K + 3) / 4 - 15 ;  --
    ; -- si = A ; dx = 19 ; bx = -(3 * K + 3) / 4 --
    imul    dx
    sub     ax, cx
    mov     cx, 30
    cwd
    idiv    cx
    ; -- si = A ; bx = -(3 * K + 3) / 4 ; dx = D --
    cmp     dx, 28
    jl      short easter_l2
    jne     short easter_l1
    cmp     si, 11
    jl      short easter_l2
easter_l1:
    dec     dx
easter_l2:
    mov     si, dx
    ; -- si = D - R ; bx = -(3 * K + 3) / 4 --
    ; Note: + 2 is from S = 2 - ...
    lea     ax, [bx + di + 2]
    mov     dx, di
    sar     dx, 1
    sar     dx, 1
    add     ax, dx
    mov     cl, 7
    cwd
    idiv    cx
    lea     ax, [si + 21 - 7]
    add     ax, dx
    cwd
    idiv    cx
    lea     ax, [si + 7 + 21]
    sub     ax, dx
    pop     si
    retn
    
year_number_to_int:
    ; -- cx - number of bytes remaining in command line --
    ; -- di - current offset in command line. Updates cx, di. --
    ; -- Return value in ax. Destroys the value of bx, dx. --
    ; -- si not modified.  --
    xor     bx, bx
year_number_to_int_loop:
    xor     ah, ah
    mov     al, [es:di]
    ; Space - end (Note that this should not occur in
    ;   first execution of loop)
    cmp     al, 20h
    je      short year_number_to_int_end
    cmp     al, '0'
    jb      short arg_parse_error
    cmp     al, '9'
    ja      short arg_parse_error
    sub     al, '0'
    xchg    ax, bx
    mov     dx, 0ah
    mul     dx
    ; mul sets CF and OF if upper 16 bits of result is not 0
    jc      short arg_parse_error
    add     bx, ax
    jc      short arg_parse_error
    cmp     bx, year_num_max
    ja      short arg_parse_error
    inc     di
    ; Decrement cx !
    loop    year_number_to_int_loop
year_number_to_int_end:
    mov     ax, bx
    cmp     ax, year_num_min
    jb      short arg_parse_error
    retn
arg_parse_error:
    ; int 21h, ah = 9h - write string
    mov     ah, 9h
    mov     dx, str_error_args
    int     21h
    ; Exit. Dos 1.X compatible, but no return code
    int     20h
    
str_error_args:
    db      'Error, argument parsing failed.'
    db      0dh, 0dh, 0ah, 24h
str_print_date_march:
    db      '00 March'
    db      24h
str_print_date_april:
    db      '00 April'
    db      24h
str_print_year:
    ; Note: due to hardcoded values / inc instruction
    ;   in the code, this must start with exactly one space
    db      ' 0000'
str_print_year_number_end:
    db      0dh, 0ah, 24h
