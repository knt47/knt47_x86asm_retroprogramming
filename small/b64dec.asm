; B64DEC - Small DOS 16-bit base64 decode tool.

; Copyright (c) 2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: fasm b64dec.asm B64DEC.COM

; Small DOS 16-bit base64 decode tool. Reads data from and writes data
;     to standard output.
; Implemented using only one bit shifts, no and/or (except for testing
;     most significant bit), etc. This may result in more efficient
;     code for 8086/8088 where the number of CPU cycles required by the
;     shl/shr instructions depends on the  number of shifts. (And it 
;     lacks "shl/shr with immediate value other than 1" instructions.)
; Note: This implementation of base64 decoding simply ignores invalid
;     characters and does not report an error.

output_line_length      equ 76

buf_input_offs          equ com_file_end
read_buf_size           equ 4000h
buf_decoded_offs        equ (buf_input_offs + read_buf_size)
sp_check                equ (buf_decoded_offs + 3000h + 400h)

use16
org 100h

prog_main:
    ; Check DOS version, at least DOS 2.y is required. In DOS 1.x, int 21h,
    ;     al = 30h is not available, it will leave al unchanged (0).
    ; int 21h, ah = 30h -> Get DOS version
    mov     ax, 3000h
    int     21h
    or      al, al
    jnz     short dos_ver_ok
    mov     ah, 9h
    mov     dx, str_err_dos_verion
    int     21h
    int     20h
dos_ver_ok:
    ; Note: If a .COM program is executed on DOS, and the available conventinal
    ;     memory is less than 64 K, the program will start but the value of sp 
    ;     will be set to a value lower than 0xfffe.
    cmp     sp, sp_check
    ja      short stack_ok
    mov     dx, str_err_memory
exit_error:
    mov     ah, 9h
    int     21h
    mov     dx, str_end_newline
    int     21h
    mov     ax, 4c01h
    int     21h
stack_ok:
    ; Push 0ffffh two times into the stack. Set bp.
    mov     bp, sp
    mov     ax, 0ffffh
    push    ax
    push    ax
    cld
file_read_loop:
    ; int 21h, ah = 3fh -> read file
    mov     ah, 3fh
    ; bx = 0 -> read from standard input
    xor     bx, bx
    mov     cx, read_buf_size
    mov     dx, buf_input_offs
    int     21h
    jnc     short file_read_loop_l1
file_read_loop_error:
    mov     dx, str_err_file_io
    jmp     exit_error
file_read_loop_l1:
    ; Check if this was a "partial" read
    ; If yes, increment byte at [bp + 4] from 0ffh to 0h
    cmp     ax, cx
    adc     [bp - 4h], bl
    mov     bx, b64_decode_lookup
    mov     di, buf_decoded_offs
    mov     si, dx
    mov     cx, ax
    call    b64_decode
    mov     bx, 1h  ; standard output handle
    cmp     [bp - 4h], bh
    jnz     short file_read_loop_l2
    call    b64_decode_remaining
file_read_loop_l2:
    mov     dx, buf_decoded_offs
    mov     cx, di
    sub     cx, dx
    ; int 21h, ah = 40h -> write to file
    mov     ah, 40h
    int     21h
    jc      short file_read_loop_error
    cmp     [bp - 4h], bh
    jnz     short file_read_loop
program_end:
    int     20h

b64_decode:
    ; Simple base64 decode routine.
    ; ds:si = the data buffer to encode
    ; es:di = output buffer
    ; ds:bx = the base64 character array. (may be changed to cs:si,
    ;     see comments)
    ; cx = size of input
    ; [bp-3h], [bp-2h], [bp-1h]: At most 24 bits of data, stored from the
    ;     previous call to the function. One byte stores 6 bits. A value of
    ;     0xff in one or more of the bytes indicates that less than 24
    ;     bits are stored.
    ; Destroys the values of all registers except bp, sp (and the segments).
    ; At exit, di will point to the byte after the end of output buffer.
    ; DF must be 0
    ; IMPORTANT: The decoder ignores invalid bytes and does not report
    ;     an error if an invalid value was found in the input.
    ; After processing the last chunk of input data with this function,
    ;     b64_decode_remaining must be called.
    ;
    ; Load the 3 bytes stored from previous run.
    ; A value of 0xff (or any other balue with the most significant byte set)
    ;     indicates that the byte is unused
    push    bp
    jcxz    b64_decode_l4
    mov     ah, [bp - 3h]
    mov     dx, [bp - 2h]
    mov     bp, bx
b64_decode_l1:
    lodsb
    or      al, al
    js      short b64_decode_l3
    ; Note: Two extra instuctions in the code, but 43 bytes smaller
    ;     lookup table
    sub     al, '+'
    jc      short b64_decode_l3
    ; Note: xlatb must be substituted on 8086/8088 if cs: prefix is needed
    ;; xlatb
    xor     bh, bh
    mov     bl, al
    add     bx, bp
    ;; mov     al, [cs:bx]
    mov     al, [bx]
    test    al, 40h
    jnz     short b64_decode_l3
    or      dh, dh
    js      short b64_decode_l2
    ; 24 bit data in buffer, write three bytes
    shl     al, 1
    shl     al, 1
    shl     ax, 1
    shl     ax, 1
    shl     ax, 1
    rcl     dl, 1
    shl     ax, 1
    rcl     dl, 1
    shl     ax, 1
    rcl     dx, 1
    shl     ax, 1
    rcl     dx, 1
    mov     al, dh
    stosb
    mov     al, dl
    stosb
    mov     al, ah
    stosb
    mov     dx, 0ffffh
    mov     ah, dl
    jmp     short b64_decode_l3
b64_decode_l2:
    mov     dh, dl
    mov     dl, ah
    mov     ah, al
b64_decode_l3:
    loop    b64_decode_l1
    pop     bp
    mov     [bp - 3h], ah
    mov     [bp - 2h], dx
b64_decode_l4:
    retn
    
b64_decode_remaining:
    ; This function must be called after processing the last chunk of data
    ;     using b64_decode.
    ; es:di = output buffer
    ; [bp-3h], [bp-2h], [bp-1h]: At most 24 bits of data, stored from the
    ;     last call of b64_decode.
    ; Destroys the value of ax, dx.
    ; At exit, di will point to the byte after the end of output buffer.
    ; DF must be 0
    mov     ah, [bp - 3h]
    mov     dx, [bp - 2h]
    ; If the same variables are used again (not in this program),
    ;     they can be cleared using the following code:
    ;; mov     dx, 0ffffh
    ;; mov     ah, dl
    ;; xchg    ah, [bp - 3h]
    ;; xchg    dx, [bp - 2h] 
    ; Nothing to do if the most significant byte of dl is set
    or      dl, dl
    js      short b64_decode_l6
    shl     ah, 1
    shl     ah, 1
    shl     ah, 1
    rcl     dl, 1
    shl     ah, 1
    rcl     dl, 1
    or      dh, dh
    js      short b64_decode_l5
    shl     ah, 1
    rcl     dx, 1
    shl     ah, 1
    rcl     dx, 1
    mov     al, dh
    stosb
b64_decode_l5:
    mov     al, dl
    stosb
b64_decode_l6:
    retn
    
b64_decode_lookup:
    ; Note: To save space, the first value in this table corresponds to
    ;     byte 0x2b ('+')
    db      62, 64, 64, 64, 63
    db      52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64
    db      64,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14
    db      15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64
    db      64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40
    db      41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64
    
str_err_memory:
    db      'Not enough conventional memory.'
    db      24h
str_err_file_io:
    db      'File I/O error.'
    db      24h
str_err_dos_verion:
    db      'DOS 2+ required.'
str_end_newline:
    db      0dh, 0dh, 0ah, 24h
com_file_end:
    
  
