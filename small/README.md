# A collection of a few, small 16-bit utilities for DOS

## B64DEC, B64ENC

Small DOS 16-bit base64 decode and encode tools. Both programs read the data from and writes data to standard output. Encoding/decoding is implemented using only one bit shifts etc. This may result in more efficient code for 8086/8088 where the number of CPU cycles required by the shl/shr instructions depends on the  number of shifts. (And it also lacks "shl/shr with immediate value other than 1" instructions.)

Note: The decoder ignores invalid characters, without reporting an error.

Usage example:

    b64enc <input.txt >encoded.b64
    b64dec <encoded.b64 >output.txt

## CECHO

A small utility that prints text under DOS using the specified foreground and background color. In DOS, ANSI.SYS provides similar functionality, but this program was designed to work even if this driver is not loaded. Usage example:

    cecho 1e:This is a test.

The 8-bit hex number before the colon is the text attribute. The first hex digit is the background (1=blue), the second is the text color (e=yellow). 
Another example, print text with color, but do not start a new line. See also the CECHOEXA.BAT file for the usage of the program in a BAT file.

    cecho.com n1e:No newline at the and
    
The program also supports the ~xx (where xx are two hexadecimal digits) syntax in the string passed as command line argument. It will print the character with the specified value from the active code page. See the source file (cecho.asm) for additional notes.
    
## EXECRESM, EXECRESP

These DOS programs execute another program with a specified amount of conventional memory (in a command line argument) reserved. For EXECRESM, the amount of memory to be specified in kilobytes, for EXECRESP in paragraphs. Usage example:

    execresm 128 TEST.EXE
    execresp 8192 TEST.EXE

The first argument is the amount of memory to reserve in Kbytes/paragraphs (see above), the second is the path to the program to execute.

## MEMSAVE

This program writes a memory dump to a file. The starting segment for the dump and the number of paragraphs to write must be specified as DOS command line arguments. Usage example:

    memsave c000+0800 outfile.bin
    
(c000 is the start segment, 0800 is the number of paragraphs to save)

## SETDATE

SETDATE changes the system date using a DOS interrupt. It is mainly intended for DOSBox as its "standard" version has no DATE command. The date must be specified in an ISO 8601 format (yyyy-mm-dd). Usage example:

    setdate 1999-12-25
    
## CAL86
    
cal86 prints the calendar for one month or one year to the DOS standard output, much like the Unix/Linux cal command line utility. FreeDos also seems to have a similar program, but this is a different implementation. It supports only the Gregorian calendar. Usage examples:

Print calendar for all 12 months in 2022:

    cal86 2022
    
Print calendar for August 2022:

    cal86 08 2022
    
If no argument is specified, the program obtains the current month and year from DOS, and a calendar will be printed for that month and year.

## EASTER86

This program calculates the date of Easter for specified year(s) according to the Gregorian calendar. Usage examples:

    easter86 2022
    easter86 2000 2020
    
In the second case, the calculation is performed for all years between 2000-2020 (inclusive). The result is printed to the DOS standard output. If no command line argument is specified, the program obtains the current year number from DOS, and calculates the date of Easter for that year.

## License, build instructions

All programs are licensed under  2-clause BSD license, see LICENSE.TXT.

The programs in this directory can be built using FASM. See the individual .ASM files for more information.

