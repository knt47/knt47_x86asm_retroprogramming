; MEMDUMP - A 16-bit, real mode memory view program

; Copyright (c) (2015) 2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build: nasm memdump.asm -o MEMDUMP.COM -fbin
;     or: fasm memdump.asm MEMDUMP.COM


; Stack variable offsets relative to bp:
; Segment and offset where the dump starts
offs_bp_dump_seg equ 2h
offs_bp_dump_start_offs equ 4h
; Value of registers at the start of the program
; Segmant and offset provided as command line argument (
;   F3 - jump to this address)
offs_bp_arg_seg equ 8h
offs_bp_arg_offs equ 6h
; Temporary variables for new segment/offset
offs_bp_new_addr_seg_temp equ 0ch
offs_bp_new_addr_offs_temp equ 0ah
; Saved value of registers at start
offs_bp_saved_ds equ 0eh
offs_bp_saved_es equ 10h
offs_bp_saved_bp equ 12h
offs_bp_saved_sp equ 14h
; Other
offs_bp_row_counter equ 15h
offs_bp_new_seg_cursor_x equ 16h
offs_bp_print_attributes equ 17h
offs_bp_cga_snow_workaround equ 18h
offs_bp_print_attributes_default_1 equ 1ah
offs_bp_print_attributes_default_2 equ 19h
; Space to reserve for stack vars
stack_vars_space_reserve equ offs_bp_print_attributes_default_1
; Offset of the start of command line arguments in the PSP
psp_offs_args_start equ 81h
; Text mode video memory segments
text_mode_vid_mem_seg equ 0b800h
vid_mem_seg_mono equ 0b000h
vid_mem_seg_mono_highbyte equ 0b0h
; Video mode for color/mono
vid_mode_text_80_25_color equ 3h 
vid_mode_text_80_25_mono equ 7h 
; Default attributes for color / mono, low byte - the dumo "window",
;  high byte - the menu on left/right side
print_attributes_default equ 711fh
print_attributes_default_mono equ 7007h
print_attributes_default_mono_lowbyte equ 7h
print_attributes_prompt_seg equ 74h
print_attributes_prompt_seg_mono equ 70h
; Error value returned by decode_hex_digit
hex_decode_error equ 0ffh

    org 100h 
    use16

prog_start:
    mov     cx, sp
    mov     dx, bp
    ; preserve sp, bp if this is not used as a standalone executable, but
    ;   instead linked into a larger progeam
    ;; push    bp
    mov     bp, sp
    sub     sp, stack_vars_space_reserve
    ; Dump now starts at the program's code segment and offset 0
    ;     if no command line arguments were specified    
    mov     [bp - offs_bp_dump_seg], cs
    xor     ax, ax
    mov     [bp - offs_bp_dump_start_offs], ax
    ; new - arg parse
    mov     [bp - offs_bp_arg_seg], ax
    mov     [bp - offs_bp_arg_offs], ax
    mov     [bp - offs_bp_saved_ds], ds
    mov     [bp - offs_bp_saved_es], es
    mov     [bp - offs_bp_saved_bp], dx
    mov     [bp - offs_bp_saved_sp], cx
    ; --- --- --- ---
    ; NEW (2): Workaround for CGA snow
    ; Waits for vertical retrace before drawing every line
    ; Not required on EGA or better, detect EGA first
    ; --- --- --- ---
    mov     [bp - offs_bp_cga_snow_workaround], al
    mov     es, ax
    ; int 10h, ah=12h, bh=10h -> get EGA/VGA information
    mov     ah, 12h
    mov     bl, 10h
    int     10h
    ; ax = video mode, dx = video memory segment, cx = default attributes
    mov     ax, vid_mode_text_80_25_color
    mov     dx, text_mode_vid_mem_seg
    mov     cx, print_attributes_default
    ; if bl did not change, then the card is CGA or MDA/Hercules
    cmp     bl, 10h
    jne     short set_vid_mem_seg
    ; Detect monochrome (MDA/Hercules)
    ;   check the first byte of color/monochrome port numer in BIOS data area
    cmp     byte [es:463h], 0b4h
    je      short set_params_mono
    inc     byte [bp - offs_bp_cga_snow_workaround]
    jmp     short set_vid_mem_seg
set_params_mono:
    ; Parameters for monochrome
    mov     al, vid_mode_text_80_25_mono
    mov     dh, vid_mem_seg_mono_highbyte
    mov     cx, print_attributes_default_mono
    
set_vid_mem_seg:
    mov     es, dx
    mov     [bp - offs_bp_print_attributes_default_1], cx
set_video_mode:
    ; Set vdeo mode - int 10h, ah=0h (mode already set)
    int     10h

    ; --- --- --- ---
    ; NEW (1): command line argument parsing
    ;     valid: 0a92 F000:FA6E
    ;     not valid: 0a000h B8000000 40:17
    ;     not valid: specifying more than one argument
    ; If no command line arg was specified or the specified
    ;     arg is invalid, the starting address defaults to cs:0
    ;     and the "user-specified address" to 0:0
    ; --- --- --- ---

parse_args:
    ; Code for parsing the segment:offset provided as command line argument
    ; restore es
    push    es
    mov     ax, cs
    mov     es, ax
    cld
    ; read command line arg from "prog. seg. prefix"
    ;    fixme, length (80h) ignored
    mov     di, psp_offs_args_start
    xor     ch, ch
    mov     cl, [di - 1h]
    ; ignore space at start
    mov     al, ' '
    repe    scasb
    ; It will always find a non-space character (the 0dh
    ;   terminator if nothing else)
    lea     si, [di - 1h]
    mov     di, -2h
l_argp1:
    mov     cx, 0ch
l_argp2:
    lodsb
    ; Note: The character read can be 0dh (terminator)
    ;   It is treated as an error.
    call    decode_hex_digit
    ; Invalid hex digit
    jc      short l_argp5
    cbw
    shl     ax, cl
    ; Add a new hex digit to either the segment or the offset
    ;   depending on the value of di
    or      [bp + di - offs_bp_arg_offs], ax
    sub     cl, 4h
    jns     short l_argp2
    inc     di
    inc     di
    ; 0 after first, 2 after second iteration of the outer loop
    jnz     short l_argp3
    ; segment has been read. two valid chars can follow: 0x0d ("terminator", 
    ;     = only a segment was specified) or ":", followed by an offset
    lodsb
    cmp     al, 0dh
    je      short l_argp4
    cmp     al, ':'
    je      short l_argp1
    jmp     short l_argp5
l_argp3:
    ; are we at the end of command line ?
    ;   Anything other than a terminator is considered an error
    lodsb
    cmp     al,0dh
    jne     short l_argp5
l_argp4:
    ; set starting segment and offset to the value stored in [bp - offs_bp_arg_seg] and
    ;   [bp - offs_bp_arg_offs]
    ;   (todo: should only modify [bp - offs_bp_dump_seg] if only a segment was provided
    mov     ax, [bp - offs_bp_arg_seg]
    mov     [bp - offs_bp_dump_seg],ax
    mov     ax, [bp - offs_bp_arg_offs]
    mov     [bp - offs_bp_dump_start_offs], ax
    ; value is ok do not clear
    jmp     short l_argp6
l_argp5:
    ; invalid value
    xor     ax, ax
    mov     word [bp - offs_bp_arg_seg], ax    
    mov     word [bp - offs_bp_arg_offs], ax
l_argp6:
    pop     es

main_loop:
    ; The main keyboard input loop.
    call    print_mem_dump
l_m1:
    ; int 10h, ah = 2h -> set cursor position
    mov     ah, 2h
    xor     bh, bh
    mov     dx, 3h
    int     10h
    ; int 16h, ah = 0h -> wait for keypress
    xor     ah, ah
    int     16h
    ;     up key
    cmp     ah, 48h
    jne     short l_m2
    sub     word [bp - offs_bp_dump_start_offs], 10h
    jmp     main_loop
    ;    down key
l_m2:
    cmp     ah, 50h
    jne     short l_m3
    add     word [bp - offs_bp_dump_start_offs], 10h
    jmp     main_loop
    ;     pg up
l_m3:
    cmp     ah, 49h
    jne     short l_m4
    sub     word [bp - offs_bp_dump_start_offs], 180h
    jmp     main_loop
    ;    pg down
l_m4:
    cmp     ah, 51h
    jne     short l_m5
    add     word [bp - offs_bp_dump_start_offs], 180h
    jmp     main_loop
    ;     enter 
l_m5:
    cmp     ah, 1ch
    jne     short l_m6
    call    prompt_new_seg
    jmp     main_loop
    ; Fixme: Far too many, should be re-implmented
    ;     using jumptable (?)
    ; f1 - truncate offs to 16 byte
l_m6:
    cmp     ah, 3bh
    jne     short l_m7
    and     word [bp - offs_bp_dump_start_offs], word 0fff0h
    jmp     main_loop
    ; f2 - set offs to 0
l_m7:
    cmp     ah, 3ch
    jne     short l_m8
    mov     word [bp - offs_bp_dump_start_offs], 0
    jmp     main_loop
    ; f3 - jump to start address:
    ;     0:0 or the command line argument
l_m8:
    cmp     ah, 3dh
    jne     short l_m9
    mov     cx, [bp - offs_bp_arg_seg]
    mov     dx, [bp - offs_bp_arg_offs]
    mov     [bp - offs_bp_dump_seg], cx
    mov     [bp - offs_bp_dump_start_offs], dx
    jmp     main_loop
    ; f4 - jump to 0:0 
l_m9:
    cmp     ah,3eh
    jne     short l_m10
    mov     word [bp - offs_bp_dump_seg], 0
    mov     word [bp - offs_bp_dump_start_offs], 0
    jmp     main_loop
    ; f5 - jump to ss:sp
l_m10:
    cmp     ah,3fh
    jne     short l_m11
    mov     cx, [bp - offs_bp_saved_sp]
    mov     dx, ss
    mov     [bp - offs_bp_dump_seg],dx
    mov     [bp - offs_bp_dump_start_offs],cx
    jmp     main_loop
    ; f6 - jump to ds:0
l_m11:
    cmp     ah,40h
    jne     short l_m12
    mov     dx, [bp - offs_bp_saved_ds]
    mov     [bp - offs_bp_dump_seg], dx
    mov     word [bp - offs_bp_dump_start_offs], 0
    jmp     main_loop 
    ; f7 - jump to cs:0
l_m12:
    cmp     ah, 41h
    jne     short l_m13
    mov     dx, cs
    mov     [bp - offs_bp_dump_seg], dx
    mov     word [bp - offs_bp_dump_start_offs], 0
    jmp     main_loop
l_m13:
    ; esc - quit
    cmp     ah, 01h
    jne     short l_m15
prog_exit:
    mov     ax, 3h
    cmp     byte [bp - offs_bp_print_attributes_default_1], \
        print_attributes_default_mono_lowbyte
    jne     short l_m14
    mov     al, vid_mode_text_80_25_mono
l_m14:    
    int     10h
    ; preserve sp, bp if this is not used as a standalone executable, but
    ;   instead linked into a larger progeam
    mov     sp, bp
    ;; pop     bp
    ; int 20h - Exit to DOS with code 0, DOS 1.X compatible
    int     20h
l_m15:
    ; Needed due to conditional jump size limit
    jmp     l_m1

print_mem_dump:
    ; Draws the memory dump, directly into the video memory.
    ; Destroys all registers except ss, sp.
    ; Top left corner of the screen
    xor     di, di 
    mov     ds, [bp - offs_bp_dump_seg]
    mov     si, [bp - offs_bp_dump_start_offs]
    ; Print 24 lines
    mov     byte [bp - offs_bp_row_counter], 18h

l_dump1:
    ; Required for avoiding "snow" on CGA
    call    wait_for_vbl
    call    start_line
    mov     [bp - offs_bp_print_attributes], ah
    mov     ax, ds
    ; First, print the segment and the offset, separated by ':'
    call    print_hex_16bit
    mov     al, ':'
    stosw
    mov     ax, si
    call    print_hex_16bit
    mov     al, 0b3h
    mov     ah, [bp - offs_bp_print_attributes_default_1]
    stosw
    ; Print 16 bytes in hex format stored at the dumped addresses
    mov     dx, 10h
l_dump2:
    lodsb
    xor     ah, ah
    mov     cl, 4h
    call    print_hex
    mov     ax, 1f20h
    stosw
    dec     dx
    jne     short l_dump2
    sub     di, 2h
    mov     al, 0b3h
    mov     ah, [bp - offs_bp_print_attributes_default_1]
    stosw
    ; On the right side of the screen, show the corresponding characters
    ;   (using the current code page)
    mov     cx, 10h
    sub     si, cx
l_dump3:
    lodsb
    stosw
    loop    l_dump3
    call    end_line
    dec     byte [bp - offs_bp_row_counter]
    jnz     short l_dump1
    call    print_regs
    mov     ax, cs
    mov     ds, ax
    ret

start_line:
    ; Draw the "box drawing" and "fill pattern" CP437
    ;   characters at the start of each line.
    mov     al, 0b1h
    mov     ah, [bp - offs_bp_print_attributes_default_2]
    stosw
    stosw
    mov     al, 0b3h
    mov     ah, [bp - offs_bp_print_attributes_default_1]
    stosw
    ret

end_line:
    ; Draw the "box drawing" and "fill pattern" CP437
    ;   characters at the end of each line.
    mov     al, 0b3h
    mov     ah, [bp - offs_bp_print_attributes_default_1]
    stosw
    mov     al, 0b1h
    mov     ah, [bp - offs_bp_print_attributes_default_2]
    stosw
    stosw
    ret

print_regs:
    ; Print the bottom row of the screen that will contain
    ;   the initial values of the segment registers plus bp, sp
    call    wait_for_vbl
    call    start_line
    mov     al, 20h
    mov     cx, 9h
    rep     stosw
    mov     al, 0b3h
    stosw
    mov     al, [bp - offs_bp_print_attributes_default_1]
    ; Yellow color ofr CGA/EGA/VGA
    and     al, 0feh
    mov     byte [bp - offs_bp_print_attributes], al
    mov     dx, 'cs'
    mov     cx, cs
    call    print_reg
    mov     dl, 'd'
    mov     cx, [bp - offs_bp_saved_ds]
    call    print_reg
    mov     dl, 'e'
    mov     cx,[bp - offs_bp_saved_es]
    call    print_reg
    mov     dl, 's'
    mov     cx, ss
    call    print_reg
    mov     dx,'bp'
    mov     cx, [bp - offs_bp_saved_bp]
    call    print_reg
    mov     dl, 's'
    mov     cx, [bp - offs_bp_saved_sp]
    call    print_reg
    sub     di, 2h
    mov     al, 0b3h
    mov     ah, [bp - offs_bp_print_attributes_default_1]
    stosw
    mov     al, 20h
    mov     cx, 10h
    rep     stosw
    call    end_line
    ret

print_reg:
    ; Prints a register in format --: ???? where -- is the two-letter
    ;   symbol for the register and ? are hex digits.
    ; dx - register name, two characters; cx - the value
    ; Destroys the value of ax, bx, cx. Preserves dx. "Advances" di.
    mov     ah, [bp - offs_bp_print_attributes]
    mov     al, dl
    stosw
    mov     al, dh
    stosw
    mov     al, ':'
    stosw
    mov     ax, cx
    call    print_hex_16bit
    mov     al, 20h
    stosw
    ret

print_hex_16bit:
    ; Prints a hexadecimal number.
    ; ax - the number to print
    ; Destroys the value of al, bx, cl. Preserves dx. "Advances" di.
    ;   (ah set to [bp - offs_bp_print_attributes])
    mov     cl, 0ch
print_hex:
    ; ax - the number to print
    ; cl = 4h -> print a sigle byte (two hex digits), cl = 0ch
    ;  -> prints 16-bit word (2 digits) 
    push    dx
    mov     dx, ax
    mov     ah, [bp - offs_bp_print_attributes]
l_print1:
    mov     bx, dx
    shr     bx, cl
    ; TODO, The 83/5 opcode may not be present on 8086/8088(?)
    and     bx, word 0fh
    mov     al, [cs:bx + hex_digits]
    stosw
    sub     cl, 4h
    jns     short l_print1
    pop     dx
    ret

prompt_new_seg:
    ; Prompts the user for entering a new address (segment first,
    ;   then offset)
    xor     ax, ax
    ; The segment and the offset will be stored first in temporary
    ;   variables. 
    mov     [bp - offs_bp_new_addr_seg_temp], ax
    mov     [bp - offs_bp_new_addr_offs_temp], ax
    mov     byte [bp - offs_bp_new_seg_cursor_x], 3h
    ; Use red text on grey background for input and
    ;   black on light background for mono (same value)
    mov     al, print_attributes_prompt_seg
    cmp     byte [bp - offs_bp_print_attributes_default_1], \
        print_attributes_default_mono_lowbyte
    jne     short l_new_seg1
    mov     al, print_attributes_prompt_seg_mono
l_new_seg1:
    mov     byte [bp - offs_bp_print_attributes], al
    ; The offset of offs_bp_new_addr_offs_temp relative to
    ;   offs_bp_new_addr_seg_temp
    mov     si, -2h
l_new_seg2:
    mov     cx, 0ch
l_new_seg3:
    ; int 10h, ah = 2h -> set cursor position
    mov     ah, 2h
    xor     bh, bh
    xor     dh, dh
    mov     dl, [bp - offs_bp_new_seg_cursor_x]
    int     10h
    call    wait_for_vbl
    mov     di, 6h
    mov     ax, [bp - offs_bp_new_addr_seg_temp]
     ; save cx into dx
    mov     dx, cx 
    call    print_hex_16bit
    ; Separator between segment and offset
    mov     al, ':'
    stosw
    mov     ax, [bp - offs_bp_new_addr_offs_temp]
    call    print_hex_16bit
    ; print offset
    mov     cx, dx
l_new_seg4:
    ; int 16h, ah = 0h -> wait for keypress 
    xor     ah, ah
    int     16h
    cmp     ah, 1h
    ; ESC pressed
    je      short l_new_seg6
    call    decode_hex_digit
    ; Invalid hex digit
    jc      short l_new_seg4
    cbw
    shl     ax, cl
    ; Add a new hex digit to either the segment or the offset
    ;   depending on the value of si
    or      [bp + si - offs_bp_new_addr_offs_temp], ax
    ; Advance the text mode cursor
    inc     byte [bp - offs_bp_new_seg_cursor_x]
    sub     cl, 4h
    ; End of the segment or the offset input?
    jns     short l_new_seg3
    ; Text mode cursor, skip the ':'
    inc     byte [bp - offs_bp_new_seg_cursor_x]
    inc     si
    inc     si
    ; 0 after first, 2 after second iteration of the outer loop
    jz      short l_new_seg2
l_new_seg5:
    ; If input is OK and the user pressed Enter,
    ;   update the address that is shown in the dump
    mov     ax, [bp - offs_bp_new_addr_seg_temp]
    mov     [bp - offs_bp_dump_seg], ax
    mov     ax, [bp - offs_bp_new_addr_offs_temp]
    mov     [bp - offs_bp_dump_start_offs], ax
l_new_seg6:
    ret
    
decode_hex_digit:
    ; New (3): hex digit decode routine
    ; Decodes a hex digit in in register al. Returns 0xff and sets
    ;   CF on error. Case insensitive, accepts '0'-'9', 'A'-'F', 'a'-'f'.
    ; The carry flag will be set if al < 30h
    sub     al, 30h
    jc      short decode_hex_error_2
    ; Is the (original value of) input <= '9' ?
    cmp     al, 9h
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    ja      short decode_hex_a_f
decode_hex_ok:
    clc
    ret
decode_hex_a_f:
    ; Digits 'A' - 'F', 'a' - 'f'
    ; Hack to support upper case with 'and'
    sub     al, 11h
    ; The carry flag will be set if the original input is < 41h
    jc      short decode_hex_error_2
    and     al, 0dfh
    add     al, 0ah
    ; Is the (original value of) input <= 'F' ?
    cmp     al, 0fh
    ; Note: we cannot rely here on the CF value set by the cmp instruction
    jbe     short decode_hex_ok
decode_hex_error_1:
    stc
decode_hex_error_2:
    mov     al, hex_decode_error
    ret
    
wait_for_vbl:
    ; Workaround for CGA snow. Waits for a VBL (vertical blanking.)
    ; TODO, this affects performance of the program significantly,
    ;   a more advanced strategy needs to be implemented later.
    ; Note: destroys the value of ax and dx
    cmp     byte [bp - offs_bp_cga_snow_workaround], 0
    ; not needed on EGA and better
    je      short wait_for_vbl3
    ; push    ax
    ; push    dx
    mov     dx, 3dah
wait_for_vbl1:
    in      al, dx
    test    al, 8h
    jnz     short wait_for_vbl1
wait_for_vbl2:
    in      al, dx
    test    al, 8h
    jz      short wait_for_vbl2
wait_for_vbl3:
    ; pop     dx
    ; pop     ax
    ret
    
hex_digits:
    db      '0123456789ABCDEF'

