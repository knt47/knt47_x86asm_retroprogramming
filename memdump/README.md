# MEMDUMP real-mode memory viewer utility

MEMDUMP is 16-bit, real mode memory viewer program. It was written in 8088/8086 compatible assembly language and supports the most important graphics adapters in IBM-compatible PCs: CGA, EGA, VGA, MDA, Hercules.

![Screenshot of MEMDUMP](../images/memdump.png "Screenshot of MEMDUMP")

*Screenshot of MEMDUMP running in DOSBox*

## Controls (keyboard)

- **Up**/**Down**: Move starting offset of dump, +/- 0x10 (hex).
- **PgUp**/**PgDn**: Move starting offset of dump, +/- 0x100 (hex).
- **Enter**: A new starting segment & offset can be entered.
- **F2**: Set offset to 0.
- **F3**: Dump memory at the address passed as a DOS command line argument. 0:0 if no argument.
- **F4**: Set starting address of dump to 0:0
- **F5**: Set starting address to SS:SP.
- **F6**: Set starting address to DS:0.
- **F7**: Set starting address to CS:0.
- **Esc**': Quit to DOS.

The starting address can be provided as a DOS command line argument in format SSSS or SSSS:OOOO where SSSS and OOOO are the segment and the offset, respectively, in hex format. If there is no command line argument, the starting address is set to DS:0.

## License

2-clause BSD license, see LICENSE.TXT.

## Build instructions

Program can be built using both FASM and NASM:

    fasm memdump.asm MEMDUMP.COM
    nasm FRCELL88.NAS -o FRC88NAS.COM -fbin
