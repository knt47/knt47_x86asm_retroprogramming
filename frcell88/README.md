# FR-CELL-88 game

FR-CELL-88 (or FRCELL88) is a simple FreeCell implementation for 8088/8086 (or newer) and DOS, with simple text-mode "graphics". It was written entirely in assembly language.

![Screenshot of FR-CELL-88](../images/frcell88.png "Screenshot of FR-CELL-88")

*Screenshot of FR-CELL-88 running in DOSBox*

## Features

- 8088/8086-compatible assembly code.
- Allows playing deals 1-65535, the first 32000 are the same as the deals in the original Microsoft game.
- Supports CGA, EGA, VGA (but CGA composite mode is problematic)
- Support for monochrome MDA/Hercules text mode. (Note: This was tested only on emulators, I have no idea how it would look like on a real monochrome monitor from the 1980s.)
- Full undo support, can undo up to 512 moves.
- Support for (["super moves"](https://solitairelaboratory.com/fcfaq.html)).
- A deal number can be passed as a DOS command line argument (e.g. FRCELL88.COM 164)

## Differences from other FreeCell implementations:

- Move numbering is different than in other implementation of FreeCell - "Super moves" are counted as single moves, but moving multiple cards on the "foundation piles" (where the aces are placed) are always treated as separate moves. (The counting of moves in fact reflects the way how undo is implemented - the number shown as "Moves" is actually the number of entries in the undo buffer.) 
- Cards are not moved automatically to the "foundation piles". The program has an "auto move" hotkey instead that moves every possible cards to the foundations. Use this feature carefully as it can move more cards to the foundations than needed, (Sometimes, this can make the game impossible to win unless the player undoes some of the moves)

## Limitations

- No sound and music.
- Keyboard only, cannot use mouse.
- The size of move/undo buffer is limited to 512 moves (however, this should be more than enough to complete all solvable deals (all except 11982) [Link](https://solitairelaboratory.com/fcfaq.html))
- Code for avoiding CGA snow is rather slow and inefficient. (But, at least, absolutely no snow is visible in PCem and 86Box)

## Controls (keyboard only)

- **1**-**8**, **B**-**E**: Select source pile cell to move card(s) from (1-8 are the piles, B-E are the free cells)
- **1**-**8**, **A**-**E**: Select destination for moving card(s) (A - move to "foundation piles")
- **U**: Undo move
- **A**: "Auto move", moves every possible cards to the foundation
- **T**: If a pile contains too many cards that can fit in a 25-line text mode screen, the cards on the top of such piles will not be visible. After pressing T, the program will show the cards on the top of the pile instead. The original view is restored when the key is pressed again.
- **F9** - new deal, prompts user for deal number
- **F10** - quit to DOS

## License

2-clause BSD license, see LICENSE.TXT.

## Build instructions

Program can be built using both FASM and NASM.

    fasm FRCEL88.FAS FRCEL88.COM
    nasm FRCELL88.NAS -o FRC88NAS.COM -fbin

For monochrome cards (MDA/Hercules), an alternative version is provided which uses a different text attribute for the red suits (hearts, diamonds), You may try this alternative version if the card graphics in poorly visible on your monitor. It can be built as:

    nasm FRCELL88.NAS -o FRC88MNG.COM -fbin -Dmono_no_gray_on_gray

(On CGA/EGA/VGA, the "alternative" version behaves like the "default" one.)
