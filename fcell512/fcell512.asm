; FCELL512 - A simplified, 512-byte (boot sector sized) implementation
;   of game FreeCell

; Copyright (c) 2021-2022 knt47
;
; Redistribution and use in source and binary forms, with or without 
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright notice,
;    this list of conditions and the following disclaimer.
;
; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.

; Build using FASM: fasm fcell512.asm
; The output is either a .bin file that can be used as boot sector
;   (dos_com_output = 0) or a DOS .COM program (dos_com_output = 1)


; dos_com_output: If 1, then the progra mis compiled as a .COM executable
;   By default (0), a .bin file generated (boot sector)
dos_com_output equ 0

; space_cols_4_5: 1 byte space between the 4th and 5th card column. 
;   Program will look (more symmetric). but 7 extra code bytes.
space_cols_4_5 equ 1

; FIXME, allow_game_restart is not implemented
allow_game_restart equ 0

start_addr_com equ 100h
start_addr_bootsect equ 7c00h

if dos_com_output > 0

start_addr = start_addr_com
start_ofs_high = 1h
card_data_ofs_high = 4h
; IMPORTANT: the less significant byte of card_data_ofs must be 0!
; 400h is used because in some previous versions the COM version was
;    513 byte due to extra restore video mode instructions, so 300h
;    would not be enough
card_data_ofs = 400h

else

start_addr = 7c00h
start_ofs_high = 7ch
card_data_ofs_high = 7bh
; IMPORTANT: the less significant byte of card_data_ofs must be 0!
card_data_ofs = 7b00h

end if

card_data_piles_ofs = card_data_ofs + 10h
deal_buf_ofs = card_data_ofs + 0b0h


pile_max_cards equ 13h  ; 19
screen_size_words equ (40 * 25)
screen_size_bytes equ (screen_size_words * 2)

; Character for filling the background of the game area 
;  (0b1h - "Fill Character, Medium") see:
; ftp://ftp.software.ibm.com/software/globalization/gcoc/attachments/CP00437.txt
; Attribute: black on blue (10h)
backgr_fill_char_attr equ 10b1h
; Video memory segment
vid_mem_seg_cga equ 0b800h

    
    use16
    org     start_addr

if dos_com_output = 0
    ; Set ds to the same segmant as cs
    push    cs
    pop     ds
end if
    ; int 10h, ah = 0h, al = 1h: Set 40x25, 16 color text mode
    mov     ax, 1h
    int     10h
    ; Set es to the segmant of CGA**EGA*VGA video memory in this mode
    mov     cx, vid_mem_seg_cga
    mov     es, cx
    ; Hide text mode cursor (int 10h, ah = 1h)
    mov     ch, 20h
    mov     ah, 1h
    int     10h
    ; Fill screen with character 0b1h, black on blue background
    mov     ax, backgr_fill_char_attr
    xor     di, di
    mov     cx, screen_size_words
    cld
    rep     stosw
    
set_rand_seed:
    ; Get system time
    xor     ah, ah
    int     1ah
    ; Random seed will be stored in cx.
    ; simple trick to limit range to 0x1-0xffff
    add     cx, dx
    adc     cx, 1
    adc     cx, 0

new_deal:
    ; deal_cards no longer a separate function as it was called from
    ;   only one place
    push    es
    push    cs
    pop     es

if allow_game_restart = 0
    mov     di, deal_buf_ofs
    mov     bx, di
else
    ; NOTE: allow_game_restart = 1 does not work!
    mov     di, rand_num_ofs
    ; store random seed (passed in ax)
    stosw
    mov     bx, di
end if
    
    ; Note: The value of a card is stored in the following way:
    ;   suit in the upper, rank in the lower nibble. The rank is numbered as:
    ;   ace = 1h, king = 0dh.
    ; Using values 0-51 would certainly reduce the size of the next loop,
    ; However, this simplifies implementing the game rules a lot. Storing
    ;   the suit into a separate nibble allows implementing opposite color
    ;   check using xor. Avoiding the use of the value of 0 for valid
    ;   cards also has advantages.
    mov     al, 3dh
deal_cards_2:
    stosb
    ; The 1-byte dec ax can be used here instead of the 2-bytes dec al, because
    ;   0 is not a valid value for cards (1h is used for the ace of hearts)
    dec     ax
    test    al, 0fh
    jnz     short deal_cards_2
    sub     al, 3
    jnc     short deal_cards_2
    ; *  the value of si is not destroyed by gen_rand! 
    mov     si, 34h
    
deal_cards_3:
    ; "shuffle" the cards, using the the Fisher-Yates shuffle algorithm
    ; Uses simple. 16-bit MLCG, values are from a paper by PIERRE L'ECUYER: 
    ; http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.99.6553&rep=rep1&type=pdf
    ;   result stored in v_rnd (and ax), also sets dx to zero.
    ;   destroys the value of dx and di, saves seed to cx.
    ; bx points to the start of the deal buffer and is not modified
    xchg    ax, cx
    mov     dx, 17364
    mul     dx
    mov     di, 65521
    div     di
    xor     ax, ax
    xchg    ax, dx
    mov     cx, ax
    ; End of random number generator
    div     si
    dec     si
    ; dx is the remainder 
    mov     di, dx
    mov     al, [bx + si]
    xchg    al, [bx + di]
    mov     [bx + si], al
    ; Note: one unnecessary run of loop, but -1 bytes
    ; Note: mov and xchg does not affect flags, ZF preserved from "dec si"
    jnz     short deal_cards_3
    ; The first 16 bytes at card_data_ofs are used for storing the cards
    ;   on the 4 "foundation piles" (ace slots) and the 4 free cells. One 
    ;   "pile"/"cell" entry consists of 2 bytes, the first is a boolean value
    ;   (0=empty, 1=there is at least one card there.) The extra 0/1 byte is
    ;   analogous with the first bytes of the pile data containing the number
    ;   of card and therefore simplifies the implementation of moving cards.
    mov     si, bx
    mov     di, card_data_ofs
    xor     ax, ax
    mov     cx, 8h
    rep     stosw
    ; "Deal" the cards into 4 piles with 7 card and another 4 with 6 cards
    mov     al, 7h
    mov     dl, 8h
deal_cards_4:
    ; Pile data stats at card_data_piles_ofs. One pile is 20 bytes. First byte
    ;   is the number of the card. A pile can contain at most 19 
    ;   (pile_max_cards) cards.
    stosb
    mov     cx, ax
    rep     movsb
    add     di, pile_max_cards
    sub     di, ax
    cmp     dl, 5h
    jne     short deal_cards_5
    dec     ax
deal_cards_5:
    ; dh is zero here, use an 1 byte smaller instruction
    dec     dx
    jnz     short deal_cards_4
    pop     es

draw_start_1:
    ; dh contains the target cell/pile 0ffh means none.
    mov     dh, 0ffh
draw_start_2:
    ; dl - loop counter, which cell/pile to draw
    mov     dl, 0fh
    ; dl position of first "ace slot"*"foundation pile" in
    ;   video memory
    mov     di, 5eah
    ; Letter 'A' indicates "ace slots" , 'C' free cells.
    mov     bl, 'A'
draw_cell_1:
    ; 'F' -> cells/ace slots selectable using F1-F8 keys.
    mov     al, 'F'
    push    bx
    call    draw_cards
    pop     bx
    jns     short draw_cell_4
    ; Sign flag set -> cell/pile empty. 
    ; Draw the "placeholder" for the empty cell, 3 rows high.
    ; This is 1 bytes shorter than calling card_col_fill_row 3 times
    mov     ax, 4f20h
    ; (?) draw_cards does not ch to 0
    mov     cx, 3h  
draw_cell_2:
    call    card_col_fill_row
    loop    draw_cell_2
draw_cell_3:
    ; Empty reed area is drawn first. letter (in bl) added later
    ; todo, long instruction, but this still seems to be the
    ;   shortest way for doing this
    mov     [es:di - 9eh], bl  
draw_cell_4:
    ; "Ace piles" (and later, free cells) are drawn from bottom to top
    ; "If the "ace piles" on the left side are done, the following
    ;    subtraction "wraps around" and CF is set. Then, start drawing the
    ;    free cells on the left side of the screen. Change letter to 'C'.
    sub     di, 2d0h
    jnc     short draw_cell_5
    mov     bl, 'C'
    mov     di, 5a0h
draw_cell_5:
    ; shorter than "dec dl" and should always work
    dec     dx
    test    dl, 8h
    jnz     short draw_cell_1
    ; Start drawing the 8 piles
    mov     di, 42h
draw_pile_1:
    mov     al, 19h  ; Down arrow symbol
    call    draw_cards
    ; clear area below cards
draw_pile_2:
    ; Fill area below the pile with the background character and color.
    mov     ax, backgr_fill_char_attr
    ; Note: We would like to compare di with screen_size_bytes.
    ;   To save 1 byte, we use screen_size_bytes + 8h instead.
    ;   The lowest possible value of di at draw_pile_3 is exactly 
    ;   screen_size_bytes + 8h so jnb will work correctly.
    mov     si, screen_size_bytes + 8h
    cmp     di, si
    jnb     short draw_pile_3
    call    card_col_fill_row
    jmp     short draw_pile_2
draw_pile_3:
    sub     di, si
if space_cols_4_5 > 0
    ; Skip one screen column between the 4th and the 5th pile for more
    ;   symmetric loop
    cmp     dl, 4h
    jne     draw_pile_4
    dec     di
    dec     di
end if
draw_pile_4:
    dec     dl
    jns     short draw_pile_1

process_key_event:
    ; int 16h, ah = 0h -> "Wait for keypress" BIOS interrupt
    xor     ah, ah
    int     16h
    mov     al, 0feh
    add     al, ah
    ; no special treatment of Esc needed, just treat it as an invalid key
    ; keys 1 - 8
    test    al, 0f8h
    jz      short process_key_5
    ; keys F1-F8 + F9, F10, (shift-F9 removed)
    sub     al, 31h
    jc      short draw_start_1
    cmp     al, 10h
    jne     short process_key_1
    ; F9 pressed, start new game
    jmp     near set_rand_seed
process_key_1:
    cmp     al, 11h
    jne     short process_key_4
    ; F10 oressed, reboot if dos_com_output = 0 (boot sector), 
    ;   reset video mode and exit to DOS if 1 (COM file)
if dos_com_output > 0
    ; restore video mode & exit
    mov     ax, 3h
    int     10h
    int     20h
else
    ; boot sector - reboot
    int     19h
end if
process_key_2:
    ; This point is unreachable
    ;   Put a jmp here due to conditional jump size limitations
    jmp     short draw_start_1

process_key_3:
    ; For smaller jump size, code from process_key_5
    mov     dh, al
    ; (Note:  Size of the follwong jump is close to -0x80)
    jmp     short draw_start_2

;; if allow_game_restart > 0
    ; FIXME, allow_game_restart no longer works
;; end if
process_key_4:
    test    al, 8h
    jz      short process_key_2
    test    al, 0f0h
    jnz     short process_key_2
process_key_5:
    ; Check for dh = 0ffh (the only possible "signed" value).
    ; If dh = 0ffh, then the source cell/pile was chosen, the
    ;   destination was not yet selected
    or      dh, dh
    js      short process_key_3
process_key_6:
move_cards_1:
    ; save al in dl, check source first
    mov     dl, al
    mov     al, dh
    ; First, removing cards from ace slots is not possible
    cmp     al, 0ch
    jnb     short process_key_2
    ; Zero flag set if the source is empty
    call    cell_pile_get_card
    jz      short process_key_2
    ; Save the card obtained in cl. Save si (pile start) in di.
    mov     cl, al
    mov     di, si
    mov     al, dl
    call    cell_pile_get_card
    ; Check if the destination is empty
    jz      short move_cards_5    
    test    dl, 8h
    jz      short move_cards_4
    ; Fail on non-empty "free cells" 
    test    dl, 4h
    jz      short process_key_2
    ; A non-empty "foundation" (ace slot), accept cards of the same suit
    ;   and one rank higher.
    inc     ax
    cmp     al, cl
move_cards_3:
    ; If ZF = 1, accept move. otherwise, reject it and wait for next keypress
    jz      short move_cards_7
    jmp     short process_key_2
move_cards_4:
    ; "Calculate" the rank that can be moved here (e.g. a 8 on a 9)
    dec     ax
    ; Check for opposite color
    xor     al, cl
    xor     al, 20h
    ; If the card moved is one rank lower and of opposite color,
    ;   "al" will be either 00h or 10h
    test    al, 2fh
    ; "Re-use" jumps in "foundation pile" code
    jmp     short move_cards_3
move_cards_5:
    ; Destination empty. Check if this is an ace slot
    cmp     dl, 0ch
    ; If yes, allow any card
    jb      short move_cards_7
    ; Otherwise, allow only aces on empty ace slots
    ; Note: This "test" would set ZF to 0 in 2 cases: 1. card is an ace
    ;   2. source is empty, however, this was already checked
    test    cl, 0eh
    jnz     short process_key_2
move_cards_7:
    ; TODO: no limit check (but 19 cards should be the 
    ;   largest possible number in normal deals(?)
    ; Remove card from source - just decrement number of cards byte
    dec     byte [di]
    ; HACK for cells/ace slots (see comment below)
    test    dl, 8h
    jnz     short move_cards_8
    ; I this was a pile, increase the number of cards byte
    inc     byte [si]
    jmp     short move_cards_9
move_cards_8:
    ; In case of "foundation piles"/"ace slots", the number of cards byte
    ;   must remain 1 if the next card was placed on a card already there
    or      byte [si], 1h
    mov     bx, si
move_cards_9:
    ; Move card to the destination cell/pile.
    mov     [bx + 1h], cl
    ; Success, continue keyboard input loop.
    jmp     short process_key_2

draw_cards:
    ; Draw either one card (for free cell/"ace slot") or the cards in a pile
    mov     ah, 1fh  ; white on blue
    ; Was the source already selected (dh != 0ffh)? If yes, are we drawing
    ;   the selected cell/pile?
    cmp     dl, dh
    jne     short draw_cards_1
    ; magenta highlight
    mov     ah, 1dh  ; light magenta on blue
draw_cards_1:
    ; The F letter (F1-F8) or the down arrow symbol
    stosw
    mov     al, dl
    and     al, 7h
    add     al, '1'
    ; The number 1 - 8
    stosw
    add     di, 4ch  ; Next wow on screen
    mov     al, dl
    ; Get number of cards in cell/pile (si will point to the
    ;   number of cards byte)
    call    cell_pile_get_card
    mov     bh, start_ofs_high + 1h
    lodsb
    mov     cl, al
    ; If there are too many cards on the pile, "collapse" them to avoid
    ;   writing after the end of the visible part of video memory.
    mov     ch, 0f5h  ; -11
    add     ch, cl
    jc      short draw_cards_2
    xor     ch, ch
draw_cards_2:
    jz      short draw_cards_5
    ; One extra card -> draw two extra cards on the top as "collapsed"
    ; TODO: One row of the screen will be left empty at the bottom
    shl     ch, 1
    sub     cl, ch
    ; ch = number of "collapsed" cards
draw_cards_3:
    ; The black line separating cards. Not drawn above the top card.
    ; Also serves as the "marker" for collapsed cards.
    mov     ax, 70c4h
draw_cards_4:
    call    card_col_fill_row
    inc     si
    dec     ch
    jnz     short draw_cards_4
draw_cards_5:
    dec     cx
    ; Note: due to the jcxz instruction before draw_cards_8, the following
    ;   conditional jump is executed only if there is only one card
    ;   (or all except the last card have been drawn)
    ;   cell drawing (draw_cell_*) relies on this for drawing the
    ;   empty row on the top
    js      short draw_cards_9
    jnz     short draw_cards_6
    mov     ax, 7020h
    call    card_col_fill_row
draw_cards_6:
    lodsb
    ; Undocumented "aam with intermediate value != 0ah" removed.
    ;   To my knowledge, this instruction works on NEC V20/V30
    ;   chips differently than on Intel 8086/8088. (TODO, reference) 
    ;   Performance of div with 8-bit register should be comparable to aam (?)
    ;; aam     10h
    ;; mov     bl, al
    ;; mov     al, ah
    cbw
    mov     bl, 10h
    div     bl
    ; Note: div is slower than bit shifts, nut for 8088/8086 this is the
    ;   implementation using the fewest possible bytes (?) (not counting the
    ;   use of aam, see the comment above)
    ; At tih point, ah = rank, al = suit
    mov     bl, ah
    mov     ah, 75h
    sub     ah, al
    and     ah, 74h
    ; ah = attribute, 74h for red suites (0x0? - hearts and 0x1? - diamonds,
    ;   70h for black ones (0x2? - clubs, 0x3? - spades)
    add     al, 3h
    ; al = corresponding symbol in CP437 (3h - 6h)
    stosw
    mov     al, 20h
    cmp     bl, 0ah
    jne     short draw_cards_7
    ; '1' digit for "10" rank
    mov     al, 31h
draw_cards_7:
    stosw
    ; Note: 101h constant in the following instruction must be modified
    ;   to 1h if the offset of the next instruction if its offset in the 
    ;   memory is 7cxxh instead of 7dxxh.
    mov     al, card_symb - start_addr - 101h
    ; Now, ah * 100h + bl is now equal to offset of card_symb - 1.
    ;   (-1 required because aces have 1h in their less significant nibble
    ;   instead of 0. Also, we have the rank of the card in bl. Next we will
    ;   use xlatb to obtain the card rank. This instruction has the interesting
    ;   property that it will do the same thing in 16-bit mode if
    ;   al and bl are swapped.
    xlatb
    stosw
    add     di, 4ah  ; Next wow on screen
    ; cx = 0 -> this is the last card, we need to draw the empty bottom row
    ;   of the card
    jcxz    draw_cards_8
    ; This is not the last card (yet). Draw one separator (ch = 1) and
    ;   compensate for the increment of si there (TODO)
    dec     si
    mov     ch, 1h
    jmp     short draw_cards_3
draw_cards_8:
    mov     al, 20h
card_col_fill_row:
    ; Draw three characters to the screen with the same attribute. Responsible
    ;   for both drawing the empty space at the bottom of the card (in this 
    ;   case, the draw_cards subroutine ends here), but also called as a 
    ;   separate routine
    stosw
    stosw
    stosw
    add     di, 4ah
    ; Note: the value of sign flag will be always cleared  because di never
    ;   reaches offsets 8000h or above.
draw_cards_9:
    retn
    
cell_pile_get_card:
    ; Returns the topmost card in a pile or the card in a free cell or
    ;   foundation pile/ace slot (if present). ZF=1 indicates that the 
    ;   pile/cell is empty.
    ; call with: al 0-7: piles (columns), 8-11: free cells, 12-15:
    ;   ace slots/"foundation piles
    ; Returns: ZF=1 if empty, ZF=0 if not empty
    ; al = the topmost card if not empty, 0 if empty
    ; si: pointer to the "number of cards" byte
    ; bx: pointer to the last card in pile if not empty, same as si if empty
    test    al, 8h
    jz      short get_card_1
    and     al, 7h
    shl     al, 1
    jmp     short get_card_2
get_card_1:
    ; Note: "aad" removed, see comments near draw_cards_6. In short, the core
    ;   would not run properly on NEC V20/V30 (?)
    ;; mov     ah, 10h
    ;; xchg    al, ah
    ;; aad     14h  ; multiply by 20 and add ah
    mov     ah, pile_max_cards + 1h
    mul     ah
    ; Value of ah not needed!
    add     al, card_data_piles_ofs - card_data_ofs
get_card_2:
    mov     ah, card_data_ofs_high
    xchg    ax, bx
    mov     si, bx  ; save bx into si
    mov     al, [bx]
    or      al, al  ; set zero flag
    jz      short get_card_3
    cbw
    add     bx, ax
    ; Note: This is not a 100% reliable method for clearing ZF:
    ;   bx may be zero if the pile data is placed at the end of a segment
    ;   due to the "wraparound". Otherwise, something like "or al, 1h" 
    ;   can be added before the "mov al, [bx]"
    mov     al, [bx]
get_card_3:
    retn
    
card_symb:
    ; The card ranks (Note: '1' for "10" is handled separately)
    db      'A234567890JQK'
    
if dos_com_output = 0
    times   510-($-$$) db 0

boot_signature:
    ; Last two bytes of the boot sector
    db      55h, 0aah
end if

