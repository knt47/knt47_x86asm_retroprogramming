# FCELL512: A simplified, 512-byte (boot sector sized) implementation of game FreeCell

FCELL512 is a tiny implementation of the FreeCell game in x86 assembly language that fits in an 512-byte boot sector. It contains only 8088/8086 compatible instructions. I have also avoided the opcodes that (to my knowledge) behave differently on NEC V20/V30 than on Intel processors (AAD, AAM, with an immediate value != 0x0a). The program uses a 40x25 text mode, therefore it works on computers with CGA/EGA/VGA cards, but it is not compatible with MDA/Hercules.

![Screenshot of FCELL512](../images/fcell512.png "Screenshot of FCELL512")

*Screenshot of FCELL512 running in DOSBox-X*

In order to fit in only 512 bytes, the program has several limitations:

- No mouse support.
- No sound effects, music.
- No numbered deals. 
- Uses a different random number generator than the original Microsoft FreeCell (but very small in x86 assembly language).
- Only one card can be moved once, no support for "super-moves". ([Link](https://solitairelaboratory.com/fcfaq.html)) Note however, in FreeCell, valid super-moves are always combinations of single card moves. Therefore, any solvable FreeCell deal can be won without support for this feature (but of course, it will probably take more time).
- Cards are not moved automatically to "foundation piles", the player needs to do this manually.
- No undo support.
- The program occupies the entire boot sector and unfortunately, no space is left for a valid FAT12 file system header.

## Controls (keyboard)

- **1**-**8**, **F1**-**F8**: Selects the source and the destination for moving a card. 1-8 are the eight "piles", F1-F4 are the four free cells, and F5-F8 are the "foundation piles"/"ace slots".
- **F9**: New, random deal.
- **F10**': Reboot (boot sector version) or quit to DOS (DOS .COM version)

## License

2-clause BSD license, see LICENSE.TXT.

## Build instructions

Program can be built using FASM:

    fasm fcell512.asm

The output is either a .bin file that can be used as boot sector (when dos_com_output = 0 is set in the fcell512.asm file) or a DOS .COM executable (when dos_com_output = 1 is used).

A 1.44 MB floppy image can be created on Linux using the following commands:

    dd if=/dev/zero of=fcell512_boot.img bs=512 count=2880 status=none
    dd if=fcell512.bin of=fcell512_boot.img bs=512 count=1 conv=notrunc status=none
